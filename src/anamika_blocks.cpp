#ifndef IMGUI_DEFINE_MATH_OPERATORS
#define IMGUI_DEFINE_MATH_OPERATORS
#endif

#include "IconsFontAwesome6.h"
#include <array>
#include <deque>
#include <fmt/core.h>
#include <fstream>
#include <imgui.h>
#include <imgui_internal.h>
#include <list>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <vector>

#include "anamika_code_gen.hpp"
#include "anamika_logos.h"
#include "topological_sort.hpp"
#include <TextEditor.h>
#include <functional>
#include <imnodes.h>
#include <iostream>
#include <utility>

#ifndef __EMSCRIPTEN__
#include "process.hpp"
#else
#include <emscripten/bind.h>
#endif

#include "json.hpp"
using json = nlohmann::json;

#include "ImGuiFileDialog.h"

extern GLuint tapenade_logo_texture;
extern GLuint inria_logo_texture;
extern GLuint cerfacs_logo_texture;
extern GLuint avatar_logo_texture;
extern GLuint anamika_logo_texture;

#ifndef __EMSCRIPTEN__
// #include <filesystem>
// namespace fs = std::filesystem;
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;
#endif

#ifdef __EMSCRIPTEN__
/**
  * @brief
  *
  * @param filename
  * @param mime_type
  * @param buffer
  * @param callback_data
  */
void handle_upload_file(
    // the filename of the file the user selected
    std::string const &filename,
    // the MIME type of the file the user selected, for example "image/png"
    std::string const &mime_type,
    // the file's content is exposed in this string_view - access the data with buffer.data() and size with buffer.size().
    std::string_view buffer,
    // optional callback data - identical to whatever you passed to handle_upload_file()
    void *callback_data = nullptr
  ) {
    std::ofstream fout(filename, std::ios_base::out|std::ios_base::binary);
    fout.write(buffer.data(), buffer.size());
    fout.close();
  }
#endif


/**
 * @brief
 *
 */
namespace ImGui {

typedef std::array<char, 16> var_name_t;
static const auto c_node_colour = ImVec4(ImColor(1, 135, 135));
static const auto c_edge_colour = ImVec4(ImColor(98, 0, 238));
// static const auto c_node_colour_link = IM_COL32(1, 135, 135, 255);
// static const auto c_edge_colour_link = IM_COL32(98, 0, 238, 255);

/**
 * @brief
 *
 */
struct PinInfo {

  /**
   * @brief
   *
   */
  void clear() {
    // Pin parameters that must be dynamically determined
    m_pinid_to_nodeid.clear();
    m_pin_in_to_outs.clear();
    m_pinid_to_type.clear();
    m_pinid_to_dim.clear();
    m_pinid_to_name.clear();
  }

  // Pin parameters that must be dynamically determined
  std::map<int, int> m_pinid_to_nodeid;
  std::map<int, std::set<int>> m_pin_in_to_outs;
  std::map<int, feature_types_t> m_pinid_to_type;
  std::map<int, int> m_pinid_to_dim;
  std::map<int, std::string> m_pinid_to_name;
};


void to_json(json& j, const PinInfo& p) {
  j = json{
    {"m_pinid_to_nodeid", p.m_pinid_to_nodeid},
    {"m_pin_in_to_outs", p.m_pin_in_to_outs},
    {"m_pinid_to_type", p.m_pinid_to_type},
    {"m_pinid_to_dim", p.m_pinid_to_dim},
    {"m_pinid_to_name", p.m_pinid_to_name}
  };
}

void from_json(const json& j, PinInfo& p) {
  j.at("m_pinid_to_nodeid").get_to(p.m_pinid_to_nodeid);
  j.at("m_pin_in_to_outs").get_to(p.m_pin_in_to_outs);
  j.at("m_pinid_to_type").get_to(p.m_pinid_to_type);
  j.at("m_pinid_to_dim").get_to(p.m_pinid_to_dim);
  j.at("m_pinid_to_name").get_to(p.m_pinid_to_name);
}

/**
 * @brief
 *
 */
enum loss_function_t { L2_LOSS = 0, L1_LOSS, TVD_LOSS };

static std::string g_graph_data_file = "";

/**
 * @brief
 *
 */
struct LossFunction {

  /**
   * @brief Construct a new Loss Function object
   *
   */
  LossFunction() {}

  /**
   * @brief
   *
   * @param cur_node_id
   * @param cur_in_pin_id
   * @param cur_out_pin_id
   */
  void render(const int cur_node_id, const int cur_in_pin_id,
              const int cur_out_pin_id) {
    auto in_pin_counter = cur_in_pin_id;
    auto node_counter = cur_node_id;
    node_id = cur_node_id;
    ImNodes::BeginNode(node_counter);
    ImNodes::BeginNodeTitleBar();
    ImNodes::BeginInputAttribute(in_pin_counter++);
    const auto str_in_pin_counter = fmt::format("{}", in_pin_counter - 1);
    ImGui::TextUnformatted(str_in_pin_counter.c_str());
    ImGui::SameLine();
    ImGui::TextUnformatted(ICON_FA_CHART_LINE " Loss Function");
    ImNodes::EndInputAttribute();
    ImNodes::EndNodeTitleBar();
    const auto node_id_text =
        fmt::format(ICON_FA_FINGERPRINT "  {}", node_counter);
    ImGui::TextUnformatted(node_id_text.c_str());
    ImNodes::EndNode();
  }

  int m_loss = L2_LOSS;
  int node_id;
};


void to_json(json& j, const LossFunction& l) {
  auto position = ImNodes::GetNodeGridSpacePos(l.node_id);
  j = json{
    {"m_loss", l.m_loss},
    {"node_id", l.node_id},
    {"position_x", position.x},
    {"position_y", position.y}
  };
}

void from_json(const json& j, LossFunction& l) {
  j.at("m_loss").get_to(l.m_loss);
  ImVec2 position;
  j.at("node_id").get_to(l.node_id);
  j.at("position_x").get_to(position.x);
  j.at("position_y").get_to(position.y);
  ImNodes::SetNodeGridSpacePos(l.node_id, position);
}

/**
 * @brief
 *
 */
struct GraphDataSource {

  /**
   * @brief Construct a new Graph Data Source object
   *
   */
  GraphDataSource() {}

  /**
   * @brief
   *
   * @param cur_node_id
   * @param cur_pin_id
   */
  void render(const int cur_node_id, const int cur_in_pin_id,
              const int cur_out_pin_id) {
    auto out_pin_counter = cur_out_pin_id;
    auto node_counter = cur_node_id;
    node_id = cur_node_id;
    ImNodes::BeginNode(node_counter);
    ImNodes::BeginNodeTitleBar();
    ImGui::TextUnformatted(ICON_FA_CIRCLE_NODES " Graph Data Source");
    ImNodes::EndNodeTitleBar();
    const auto node_id_text =
        fmt::format(ICON_FA_FINGERPRINT "  {}", node_counter);
    ImGui::TextUnformatted(node_id_text.c_str());
    render_node(out_pin_counter);
    out_pin_counter -= m_node_var_name.size();

    ImGui::Dummy(ImVec2(0.0f, 20.0f));

    render_edge(out_pin_counter);
    ImNodes::EndNode();
  }

  /**
   * @brief
   *
   * @param cur_out_pin_id
   */
  void render_node(const int cur_out_pin_id) {
    auto out_pin_counter = cur_out_pin_id;
    ImGui::PushStyleColor(ImGuiCol_Button, c_node_colour);
    ImGui::PushStyleColor(ImGuiCol_FrameBg, c_node_colour);
    if (ImGui::Button("  Node Features  ")) {
    }
    // Node dimension input box
    ImGui::Text(ICON_FA_LIST_OL);
    if (ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenDisabled)) {
      ImGui::SetTooltip("Total number of features");
    }
    ImGui::SameLine();
    ImGui::PushItemWidth(70);
    ImGui::InputInt("##Number of node features", &m_num_node_vars);
    ImGui::PopItemWidth();
    if (m_num_node_vars <= 0)
      m_num_node_vars = 1;
    if (m_num_node_vars > 99)
      m_num_node_vars = 99;
    m_node_var_dim.resize(m_num_node_vars);
    m_node_var_name.resize(m_num_node_vars, {'\0'});
    if (ImGui::Button("  Node Feature Attributes  ")) {
    }
    for (unsigned i = 0; i < m_node_var_dim.size(); ++i) {
      if (m_node_var_dim[i] > 99)
        m_node_var_dim[i] = 99;
      if (m_node_var_dim[i] <= 0)
        m_node_var_dim[i] = 1;
      // Node feature output conduit
      ImNodes::BeginOutputAttribute(out_pin_counter--);
      auto some = fmt::format("NodeFeature{}", i + 1);
      if (std::string(m_node_var_name[i].data()).empty())
        std::copy_n(some.begin(), some.size(), m_node_var_name[i].begin());
      some.insert(0, "##");
      ImGui::Text(ICON_FA_ARROWS_LEFT_RIGHT_TO_LINE);
      if (ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenDisabled)) {
        ImGui::SetTooltip("Resize feature vector length");
      }
      ImGui::SameLine();
      ImGui::PushItemWidth(70);
      ImGui::InputInt(some.c_str(), m_node_var_dim.data() + i);
      ImGui::PopItemWidth();
      ImGui::PushItemWidth(120);
      ImGui::SameLine();
      ImGui::Text(ICON_FA_USER);
      if (ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenDisabled)) {
        ImGui::SetTooltip(
            "Specify the name of the feature\n vector (must be unique)");
      }
      ImGui::SameLine();
      some.insert(some.end(), 'N');
      ImGui::InputText(some.c_str(), m_node_var_name[i].data(), 16);
      ImGui::PopItemWidth();
      ImGui::PushItemWidth(70);
      ImGui::SameLine();
      const auto str_out_pin_counter = fmt::format("{}", out_pin_counter + 1);
      ImGui::TextUnformatted(str_out_pin_counter.c_str());
      ImNodes::EndOutputAttribute();
    }
    ImGui::PopStyleColor();
    ImGui::PopStyleColor();
  }

  /**
   * @brief
   *
   * @param cur_out_pin_id
   */
  void render_edge(const int cur_out_pin_id) {
    auto out_pin_counter = cur_out_pin_id;
    ImGui::PushStyleColor(ImGuiCol_Button, c_edge_colour);
    ImGui::PushStyleColor(ImGuiCol_FrameBg, c_edge_colour);
    if (ImGui::Button("  Edge Features  ")) {
    }
    // Edge dimension input box
    ImGui::Text(ICON_FA_LIST_OL);
    if (ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenDisabled)) {
      ImGui::SetTooltip("Total number of features");
    }
    ImGui::SameLine();
    ImGui::PushItemWidth(70);
    ImGui::InputInt("##Number of edge features", &m_num_edge_vars);
    ImGui::PopItemWidth();
    if (m_num_edge_vars <= 0)
      m_num_edge_vars = 1;
    if (m_num_edge_vars > 99)
      m_num_edge_vars = 99;
    m_edge_var_dim.resize(m_num_edge_vars);
    m_edge_var_name.resize(m_num_edge_vars, {'\0'});
    ImGui::SameLine();
    ImGui::Text(ICON_FA_ARROW_RIGHT_ARROW_LEFT);
    if (ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenDisabled)) {
      ImGui::SetTooltip("Toggle for symmetric graphs");
    }
    ImGui::SameLine();
    ImGui::Checkbox("##IsSymmetricEdge", &(m_is_symmetric_edge));

    if (ImGui::Button("  Edge Feature Attributes  ")) {
    }
    for (unsigned i = 0; i < m_edge_var_dim.size(); ++i) {
      if (m_edge_var_dim[i] > 99)
        m_edge_var_dim[i] = 99;
      if (m_edge_var_dim[i] <= 0)
        m_edge_var_dim[i] = 1;
      // Edge feature output conduit
      ImNodes::BeginOutputAttribute(out_pin_counter--);
      auto some = fmt::format("EdgeFeature{}", i + 1);
      if (std::string(m_edge_var_name[i].data()).empty())
        std::copy_n(some.begin(), some.size(), m_edge_var_name[i].begin());
      some.insert(0, "##");
      ImGui::Text(ICON_FA_ARROWS_LEFT_RIGHT_TO_LINE);
      if (ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenDisabled)) {
        ImGui::SetTooltip("Resize feature vector length");
      }
      ImGui::SameLine();
      ImGui::PushItemWidth(70);
      ImGui::InputInt(some.c_str(), m_edge_var_dim.data() + i);
      ImGui::PopItemWidth();
      ImGui::PushItemWidth(120);
      ImGui::SameLine();
      ImGui::Text(ICON_FA_USER);
      if (ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenDisabled)) {
        ImGui::SetTooltip(
            "Specify the name of the feature\n vector (must be unique)");
      }
      ImGui::SameLine();
      some.insert(some.end(), 'N');
      ImGui::InputText(some.c_str(), m_edge_var_name[i].data(), 16);
      ImGui::PopItemWidth();
      ImGui::SameLine();
      const auto str_out_pin_counter = fmt::format("{}", out_pin_counter + 1);
      ImGui::TextUnformatted(str_out_pin_counter.c_str());
      ImNodes::EndOutputAttribute();
    }
    ImGui::PopStyleColor();
    ImGui::PopStyleColor();
  }

public:
  int m_num_node_vars = 1;
  std::vector<int> m_node_var_dim;
  std::vector<var_name_t> m_node_var_name;

  int m_num_edge_vars = 1;
  bool m_is_symmetric_edge = false;
  std::vector<int> m_edge_var_dim;
  std::vector<var_name_t> m_edge_var_name;
  int node_id;
};

void to_json(json& j, const GraphDataSource& g) {
  auto position = ImNodes::GetNodeGridSpacePos(g.node_id);
  j = json{
    {"m_num_node_vars", g.m_num_node_vars},
    {"m_node_var_dim", g.m_node_var_dim},
    {"m_node_var_name", g.m_node_var_name},

    {"m_num_edge_vars", g.m_num_edge_vars},
    {"m_is_symmetric_edge", g.m_is_symmetric_edge},
    {"m_edge_var_dim", g.m_edge_var_dim},
    {"m_edge_var_name", g.m_edge_var_name},
    {"node_id", g.node_id},
    {"position_x", position.x},
    {"position_y", position.y}
  };
}

void from_json(const json& j, GraphDataSource& g) {
  j.at("m_num_node_vars").get_to(g.m_num_node_vars);
  j.at("m_node_var_dim").get_to(g.m_node_var_dim);
  j.at("m_node_var_name").get_to(g.m_node_var_name);

  j.at("m_num_edge_vars").get_to(g.m_num_edge_vars);
  j.at("m_is_symmetric_edge").get_to(g.m_is_symmetric_edge);
  j.at("m_edge_var_dim").get_to(g.m_edge_var_dim);
  j.at("m_edge_var_name").get_to(g.m_edge_var_name);
  ImVec2 position;
  j.at("node_id").get_to(g.node_id);
  j.at("position_x").get_to(position.x);
  j.at("position_y").get_to(position.y);
  ImNodes::SetNodeGridSpacePos(g.node_id, position);
}

/**
 * @brief
 *
 */
struct EncoderNode {

  /**
   * @brief Construct a new Encoder Node object
   *
   */
  EncoderNode() {}

  /**
   * @brief
   *
   */
  void render(const int cur_node_id, const int cur_in_pin_id,
              const int cur_out_pin_id) {
    auto in_pin_counter = cur_in_pin_id;
    auto out_pin_counter = cur_out_pin_id;
    auto node_counter = cur_node_id;
    node_id = cur_node_id;
    ImNodes::BeginNode(node_counter++);
    ImNodes::BeginNodeTitleBar();
    ImNodes::BeginInputAttribute(in_pin_counter++);
    const auto str_in_pin_counter = fmt::format("{}", in_pin_counter - 1);
    ImGui::TextUnformatted(str_in_pin_counter.c_str());
    ImGui::SameLine();
    ImGui::TextUnformatted(ICON_FA_NETWORK_WIRED " Encoder");
    ImNodes::EndInputAttribute();
    ImNodes::EndNodeTitleBar();
    const auto node_id_text =
        fmt::format(ICON_FA_FINGERPRINT "  {}", node_counter - 1);
    ImGui::TextUnformatted(node_id_text.c_str());
    if (ImGui::Button("  Encoder Attributes  ")) {
    }
    // Number of layers
    ImGui::Text(ICON_FA_LIST_OL);
    if (ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenDisabled))
      ImGui::SetTooltip("Total number of MLP layers");
    ImGui::SameLine();
    ImGui::PushItemWidth(70);
    ImGui::InputInt("##Number of layers", &encoder_layer_dim);
    ImGui::PopItemWidth();
    ImGui::SameLine();
    ImGui::Text(ICON_FA_USER);
    ImGui::SameLine();
    if (std::string(encoder_name.data()).empty()) {
      const auto some = fmt::format("Encoder_{}", node_counter - 1);
      std::copy_n(some.begin(), some.size(), encoder_name.begin());
    }
    static auto encoder_hash = fmt::format("##{}", encoder_name.data());
    ImGui::PushItemWidth(120);
    ImGui::InputText(encoder_hash.c_str(), encoder_name.data(), 16);
    ImGui::PopItemWidth();
    // ImGui::SameLine();
    ImGui::Text(ICON_FA_ARROWS_TO_CIRCLE);
    if (ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenDisabled))
      ImGui::SetTooltip("Scatter edge features to \nnodes (only applies to "
                        "MLP\nwith edge input featues)");
    ImGui::SameLine();
    static auto encoder_scatter_hash = fmt::format("{}Scatter", encoder_hash);
    ImGui::Checkbox(encoder_scatter_hash.c_str(), &(scatter_after_encode));
    if (scatter_after_encode) {
      static auto encoder_scatter_type_hash =
          fmt::format("{}ScatterFun", encoder_hash);
      ImGui::SameLine();
      ImGui::PushItemWidth(70);
      ImGui::Combo(encoder_scatter_type_hash.c_str(), (int *)&scatter_type,
                   C_ScatterFunctionNames, IM_ARRAYSIZE(C_ScatterFunctionNames),
                   IM_ARRAYSIZE(C_ScatterFunctionNames));
      ImGui::PopItemWidth();
    }
    if (encoder_layer_dim <= 0)
      encoder_layer_dim = 1;
    if (encoder_layer_dim > 99)
      encoder_layer_dim = 99;
    encoder_dim.resize(encoder_layer_dim);
    encoder_activation.resize(encoder_layer_dim);
    encoder_normalise.resize(encoder_layer_dim);
    if (ImGui::Button("  Layer Attributes  ")) {
    }
    for (unsigned i = 0; i < encoder_dim.size(); ++i) {
      if (encoder_dim[i] > 99)
        encoder_dim[i] = 99;
      if (encoder_dim[i] <= 0)
        encoder_dim[i] = 1;
      if (i < encoder_dim.size() - 1) {
        ImGui::Text(ICON_FA_ARROWS_LEFT_RIGHT_TO_LINE);
        if (ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenDisabled))
          ImGui::SetTooltip(
              "Resize feature vector length\nof current MLP layer");
        ImGui::SameLine();
        ImGui::PushItemWidth(70);
        const auto some = fmt::format("##SzLayer{}", i + 1);
        ImGui::InputInt(some.c_str(), encoder_dim.data() + i);
        const auto some2 = fmt::format("##ActLayer{}", i + 1);
        ImGui::SameLine();
        ImGui::Text("f(" ICON_FA_WAVE_SQUARE ")");
        if (ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenDisabled)) {
          ImGui::SetTooltip("Activation function for\ncurrent MLP layer");
        }
        ImGui::SameLine();
        ImGui::PushItemWidth(70);
        ImGui::Combo(some2.c_str(), &(encoder_activation[i]),
                     C_ActivationFunctionNames,
                     IM_ARRAYSIZE(C_ActivationFunctionNames),
                     IM_ARRAYSIZE(C_ActivationFunctionNames));
        ImGui::PopItemWidth();
        ImGui::SameLine();
        ImGui::Text(ICON_FA_SQUARE_ROOT_VARIABLE);
        if (ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenDisabled)) {
          ImGui::SetTooltip("Toggle layer normalisation");
        }
        ImGui::SameLine();
        const auto some1 = fmt::format("##NrmLayer{}", i + 1);
        ImGui::Checkbox(some1.c_str(), &(encoder_normalise[i]));
        ImGui::PopItemWidth();
      } else {
        // Edge feature output conduit
        ImNodes::BeginOutputAttribute(out_pin_counter--);
        ImGui::Text(ICON_FA_ARROWS_LEFT_RIGHT_TO_LINE);
        if (ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenDisabled))
          ImGui::SetTooltip(
              "Resize feature vector length\nof current MLP layer");
        ImGui::SameLine();
        ImGui::PushItemWidth(70);
        const auto some = fmt::format("##SzLayer{}", i + 1);
        ImGui::InputInt(some.c_str(), encoder_dim.data() + i);
        const auto some2 = fmt::format("##ActLayer{}", i + 1);
        ImGui::SameLine();
        ImGui::Text("f(" ICON_FA_WAVE_SQUARE ")");
        if (ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenDisabled))
          ImGui::SetTooltip("Activation function for\ncurrent MLP layer");
        ImGui::SameLine();
        ImGui::PushItemWidth(70);
        ImGui::Combo(some2.c_str(), (int *)&(encoder_activation[i]),
                     C_ActivationFunctionNames,
                     IM_ARRAYSIZE(C_ActivationFunctionNames),
                     IM_ARRAYSIZE(C_ActivationFunctionNames));
        ImGui::PopItemWidth();
        ImGui::SameLine();
        ImGui::Text(ICON_FA_SQUARE_ROOT_VARIABLE);
        if (ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenDisabled))
          ImGui::SetTooltip("Toggle layer normalisation");
        ImGui::SameLine();
        const auto some1 = fmt::format("##NrmLayer{}", i + 1);
        ImGui::Checkbox(some1.c_str(), &(encoder_normalise[i]));
        ImGui::PopItemWidth();
        ImGui::SameLine();
        ImGui::TextUnformatted(std::to_string(out_pin_counter + 1).c_str());
        ImNodes::EndOutputAttribute();
      }
    }
    if (param_size > 0) {
      const auto str_param_size = fmt::format("NumParams = {}", param_size);
      ImGui::Text("%s", str_param_size.c_str());
    }
    ImNodes::EndNode();
  }

  bool scatter_after_encode = false;
  scatter_function_t scatter_type;
  var_name_t encoder_name = {'\0'};
  int input_dim = 0;
  int param_size = 0;
  std::vector<int> encoder_dim;
  std::vector<int> encoder_activation;
  std::deque<bool> encoder_normalise;
  int encoder_layer_dim = 1;
  int node_id;
};

void to_json(json& j, const EncoderNode& e) {
  auto position = ImNodes::GetNodeGridSpacePos(e.node_id);
  j = json{
    {"scatter_after_encode", e.scatter_after_encode},
    {"scatter_type", e.scatter_type},
    {"encoder_name", e.encoder_name},
    {"input_dim", e.input_dim},
    {"param_size", e.param_size},
    {"encoder_dim", e.encoder_dim},
    {"encoder_activation", e.encoder_activation},
    {"encoder_normalise", e.encoder_normalise},
    {"encoder_layer_dim", e.encoder_layer_dim},
    {"node_id", e.node_id},
    {"position_x", position.x},
    {"position_y", position.y}
  };
}

void from_json(const json& j, EncoderNode& e) {
  j.at("scatter_after_encode").get_to(e.scatter_after_encode);
  j.at("scatter_type").get_to(e.scatter_type);
  j.at("encoder_name").get_to(e.encoder_name);
  j.at("input_dim").get_to(e.input_dim);
  j.at("param_size").get_to(e.param_size);
  j.at("encoder_dim").get_to(e.encoder_dim);
  j.at("encoder_activation").get_to(e.encoder_activation);
  j.at("encoder_normalise").get_to(e.encoder_normalise);
  j.at("encoder_layer_dim").get_to(e.encoder_layer_dim);
  ImVec2 position;
  j.at("node_id").get_to(e.node_id);
  j.at("position_x").get_to(position.x);
  j.at("position_y").get_to(position.y);
  ImNodes::SetNodeGridSpacePos(e.node_id, position);
}

/**
 * @brief
 *
 */
struct GraphNetworkModel {

  /**
   * @brief
   *
   * @return int
   */
  int add_encoder() { return 0; }

  /**
   * @brief
   *
   */
  int remap_pinid_to_nodeid() {
    clear_dynamic_data();
    int in_pin_counter = 1;
    int out_pin_counter = -1;
    int node_counter = 1;
    int last_pinid = 0;
    // Add the pins to the current node id (graph data source)
    // m_pinid_to_nodeid[in_pin_counter++] = node_counter;
    // Node feature pins go first
    for (unsigned i = 0; i < m_graph_source.m_node_var_dim.size(); ++i) {
      m_pin_info.m_pinid_to_nodeid[out_pin_counter] = node_counter;
      m_pin_info.m_pinid_to_type.insert({out_pin_counter, C_FEAT_NODE});
      m_pin_info.m_pinid_to_name[out_pin_counter] =
          m_graph_source.m_node_var_name[i].data();
      m_pin_info.m_pinid_to_dim[out_pin_counter] =
          m_graph_source.m_node_var_dim[i];
      out_pin_counter--;
    }
    // Edge feature pins go next
    auto default_edge_feat = C_FEAT_EDGE_ASYMMETRIC;
    if (m_graph_source.m_is_symmetric_edge)
      default_edge_feat = C_FEAT_EDGE_SYMMETRIC;
    for (unsigned i = 0; i < m_graph_source.m_edge_var_dim.size(); ++i) {
      m_pin_info.m_pinid_to_nodeid[out_pin_counter] = node_counter;
      m_pin_info.m_pinid_to_type.insert({out_pin_counter, default_edge_feat});
      m_pin_info.m_pinid_to_name[out_pin_counter] =
          m_graph_source.m_edge_var_name[i].data();
      m_pin_info.m_pinid_to_dim[out_pin_counter] =
          m_graph_source.m_edge_var_dim[i];
      out_pin_counter--;
    }
    last_pinid = out_pin_counter + 1;
    node_counter++;
    // Add the encoder pins map
    for (unsigned i = 0; i < m_encoders.size(); ++i) {
      auto i_enc = m_encoders.begin();
      std::advance(i_enc, i);
      // Output of encoder
      m_pin_info.m_pinid_to_nodeid[out_pin_counter] = node_counter;
      m_pin_info.m_pinid_to_name[out_pin_counter] =
          fmt::format("xout{}", std::abs(out_pin_counter));
      m_pin_info.m_pinid_to_dim[out_pin_counter] = i_enc->encoder_dim.back();
      out_pin_counter--;
      // Input of encoder
      m_pin_info.m_pinid_to_nodeid[in_pin_counter] = node_counter;
      m_pin_info.m_pinid_to_name[in_pin_counter] =
          fmt::format("xin{}", std::abs(in_pin_counter));
      m_pin_info.m_pinid_to_dim[in_pin_counter] = 0;
      in_pin_counter++;
      // Increment the node counter
      node_counter++;
    }
    // Add loss pin
    m_pin_info.m_pinid_to_nodeid[in_pin_counter] = node_counter;
    m_pin_info.m_pinid_to_name[in_pin_counter] =
        fmt::format("xin{}", std::abs(in_pin_counter));
    in_pin_counter++;

    // Map nodeids to the blocks
    m_nodeid_pinids.resize(node_counter);
    for (const auto &item : m_pin_info.m_pinid_to_nodeid)
      m_nodeid_pinids[item.second - 1].insert(item.first);
    return last_pinid;
  }

  /**
   * @brief Create a node graph object
   *
   * @return true
   * @return false
   */
  bool create_node_graph() {
    // Adjncy structure of symmetric graph of Neural network blocks
    m_adjncy_blocks.resize(2 + m_encoders.size());
    for (const auto &item : m_links) {
      const auto &pin_out = item.first;
      for (const auto &pin_in : item.second) {
        m_adjncy_blocks[m_pin_info.m_pinid_to_nodeid[pin_out] - 1].insert(
            m_pin_info.m_pinid_to_nodeid[pin_in] - 1);
        m_pin_info.m_pin_in_to_outs[pin_in].insert(pin_out);
      }
    }
    auto is_node_graph_cyclic = is_cyclic(m_adjncy_blocks);
    if (!is_node_graph_cyclic)
      m_block_sequence = topological_sort(m_adjncy_blocks);
    return is_node_graph_cyclic;
  }

  /**
   * @brief
   *
   */
  template <typename T_Message> void remap_pinid_to_type(T_Message &message) {
    std::string str_msg_log = message.GetText();
    m_node_feat_type.resize(m_block_sequence.size(), C_FEAT_UNKNOWN);
    // 1. Loop over the topologically sorted sequence
    for (const auto &nodeid : m_block_sequence) {
      // Storage for the input feature types of current node in block seq
      bool is_symmetric_edge = false;
      bool has_node_feat = false;
      bool has_edge_feat = false;
      // 2. Loop over all pins of this node
      for (const auto &pinid : m_nodeid_pinids[nodeid]) {
        // 3. Look for all incoming pins to the current input pin
        if (pinid > 0) {
          // 4. Loop over all incoming pins (as outputs of previous nodes)
          for (const auto &pinin : m_pin_info.m_pin_in_to_outs[pinid]) {
            const auto &found_type = m_pin_info.m_pinid_to_type.find(pinin);
            if (found_type == std::end(m_pin_info.m_pinid_to_type))
              throw std::runtime_error(
                  "Error: Could not find pin type ... please check if your "
                  "model is fully connected");
            // 4. If any one of the pin is an edge type then assign the output
            // of encode to edge and if an unknown feature type is detected
            // throw an error
            switch (found_type->second) {

            case C_FEAT_MIXED_NODE_EDGE_SYMMETRIC:
              has_node_feat = true;
              [[fallthrough]];
            case C_FEAT_EDGE_SYMMETRIC:
              is_symmetric_edge = true;
              has_edge_feat = true;
              break;

            case C_FEAT_MIXED_NODE_EDGE_ASYMMETRIC:
              has_node_feat = true;
              [[fallthrough]];
            case C_FEAT_EDGE_ASYMMETRIC:
              has_edge_feat = true;
              break;

            case C_FEAT_NODE:
              has_node_feat = true;
              break;

            default:
              str_msg_log.append(std::string("Error: Unknown edge feature\n"));
            }
          }
          ///////////////////////////////////////////////////////////////////////////
          // The input pin feature
          ///////////////////////////////////////////////////////////////////////////
          feature_types_t in_feat = C_FEAT_UNKNOWN;
          if (has_node_feat)
            in_feat = C_FEAT_NODE;
          if (has_edge_feat && is_symmetric_edge)
            in_feat = C_FEAT_EDGE_SYMMETRIC;
          if (has_edge_feat && !is_symmetric_edge)
            in_feat = C_FEAT_EDGE_ASYMMETRIC;
          if (has_node_feat && has_edge_feat && is_symmetric_edge)
            in_feat = C_FEAT_MIXED_NODE_EDGE_SYMMETRIC;
          if (has_node_feat && has_edge_feat && !is_symmetric_edge)
            in_feat = C_FEAT_MIXED_NODE_EDGE_ASYMMETRIC;
          if (in_feat == C_FEAT_UNKNOWN)
            throw std::runtime_error(
                "Error: Input pin has C_FEAT_UNKNOWN type ... please check if "
                "your model is fully connected");
          m_pin_info.m_pinid_to_type.insert(
              {pinid, in_feat}); // Insert the type to the pin map
        }
      } // only input pin if condition ends

      ///////////////////////////////////////////////////////////////////////////
      // 5. Check if the present node is an encoder node if so
      //    determine the feature output type for the encoder pin
      if (nodeid > 0 && nodeid < m_block_sequence.size() - 1) {

        // By default set the type to edge and change it based on some
        // conditions to follow
        auto cur_node_out_feat = C_FEAT_UNKNOWN;
        if (is_symmetric_edge)
          cur_node_out_feat = C_FEAT_EDGE_SYMMETRIC;
        else
          cur_node_out_feat = C_FEAT_EDGE_ASYMMETRIC;

        // Pure node feature inputs
        if (!has_edge_feat && has_node_feat)
          cur_node_out_feat = C_FEAT_NODE;

        // Mixed node-edge input with scatter should have a node feature as
        // output
        if (has_edge_feat && has_node_feat) {
          auto encoder = m_encoders.begin();
          std::advance(encoder, nodeid - 1);
          if (encoder->scatter_after_encode == true) {
            cur_node_out_feat = C_FEAT_NODE;
            str_msg_log.append(
                "Found scatter to node so setting type to Node\n");
          }
        }

        // Set the encoders feature and output pin feature
        m_node_feat_type[nodeid] = cur_node_out_feat;

        // 6. Now set the feature type for the output pin of this encoder
        for (const auto &pinid : m_nodeid_pinids[nodeid])
          if (pinid < 0)
            m_pin_info.m_pinid_to_type[pinid] = m_node_feat_type[nodeid];
      }
      ///////////////////////////////////////////////////////////////////////////
      // Print node feat type
      str_msg_log.append(fmt::format("Node #{} feature type is {}\n", nodeid,
                                     (int)m_node_feat_type[nodeid]));
      ///////////////////////////////////////////////////////////////////////////
    }
    message.SetText(str_msg_log);
  }

  /**
   * @brief
   *
   * @tparam T_Message
   * @param message
   */
  template <typename T_Message> void recalc_param_sizes(T_Message &message) {
    auto str_msg = message.GetText();
    for (unsigned i = 0; i < m_encoders.size(); ++i) {
      auto enc = m_encoders.begin();
      std::advance(enc, i);
      enc->input_dim = 0;
      enc->param_size = 0;
      int node_fac = 1;
      // Add all the input features
      for (auto &item : m_nodeid_pinids[i + 1]) {
        auto found_type = m_pin_info.m_pinid_to_type.find(item);
        if (found_type == std::end(m_pin_info.m_pinid_to_type))
          throw std::runtime_error(
              "Error: Could not deduce pin type ... please check if your model "
              "is fully connected");
        if (found_type->second == C_FEAT_MIXED_NODE_EDGE_SYMMETRIC ||
            found_type->second == C_FEAT_MIXED_NODE_EDGE_ASYMMETRIC)
          node_fac = 2;
        // Look for input pins to determine the sizes
        if (item > 0) {
          for (auto &pinin : m_pin_info.m_pin_in_to_outs[item]) {
            auto found_in_type = m_pin_info.m_pinid_to_type.find(pinin);
            if (found_in_type == std::end(m_pin_info.m_pinid_to_type))
              throw std::runtime_error(
                  "Error: Could not deduce pin type ... please check if your "
                  "model is fully connected");
            switch (found_in_type->second) {

            case C_FEAT_EDGE_SYMMETRIC:
              [[fallthrough]];
            case C_FEAT_EDGE_ASYMMETRIC:
              enc->input_dim += m_pin_info.m_pinid_to_dim[pinin];
              break;

            case C_FEAT_NODE:
              enc->input_dim += m_pin_info.m_pinid_to_dim[pinin] * node_fac;
              [[fallthrough]];
            default:
              break;
            }
          }
        }
      }

      // Determine the param size now that input pin dim is determined
      enc->param_size += enc->input_dim * enc->encoder_dim[0] +
                         enc->encoder_dim[0] +
                         ((enc->encoder_normalise[0]) ? (2 * enc->encoder_dim[0]) : 0);
      for (unsigned i = 1; i < enc->encoder_dim.size(); ++i)
        enc->param_size += enc->encoder_dim[i - 1] * enc->encoder_dim[i] +
                           enc->encoder_dim[i] +
                           ((enc->encoder_normalise[i]) ? (2 * enc->encoder_dim[i]) : 0);

      // Output to message window
      str_msg.append(fmt::format("Encoder {} param size is and input dim is \n",
                                 i + 1, enc->param_size, enc->input_dim));
    }
    message.SetText(str_msg);
  }

  /**
   * @brief
   *
   * @return std::string
   */
  template <typename T_Message> void generate_code(T_Message &message) {
    try {
      m_inference_source.clear();
      m_inference_source_cplx.clear();
      message.SetText(std::string(""));
      remap_pinid_to_nodeid();
      auto is_node_graph_cyclic = create_node_graph();
      if (!is_node_graph_cyclic) {
        remap_pinid_to_type(message);
        recalc_param_sizes(message);
      }
      std::string str_msg_log = message.GetText();
      str_msg_log.append("Remapping pin-ids to node-ids\n");
      // Print error messages
      for (const auto &item : m_pin_info.m_pinid_to_nodeid)
        str_msg_log.append(
            fmt::format("Pin({}) maps to Node({})\n", item.first, item.second));
      unsigned count = 1;
      for (const auto &item : m_nodeid_pinids) {
        str_msg_log.append(fmt::format("Node({}) contains Pins(", count++));
        for (const auto &pin : item)
          str_msg_log.append(fmt::format("{}, ", pin));
        str_msg_log.append(");\n");
      }
      // Print input to output pins
      str_msg_log.append("Pin in to outs\n");
      for (const auto &item : m_pin_info.m_pin_in_to_outs) {
        str_msg_log.append(fmt::format("{} -> ", item.first));
        for (const auto &pinout : item.second) {
          str_msg_log.append(fmt::format("{}", pinout));
          if (&(pinout) != &(*item.second.rbegin()))
            str_msg_log.append(", ");
        }
        str_msg_log.append("\n");

        str_msg_log.append(
            fmt::format("{} -> ", m_pin_info.m_pinid_to_name[item.first]));
        for (const auto &pinout : item.second) {
          str_msg_log.append(m_pin_info.m_pinid_to_name[pinout]);
          if (&(pinout) != &(*item.second.rbegin()))
            str_msg_log.append(", ");
        }
        str_msg_log.append("\n");
      }

      str_msg_log.append("Adjacency structure\n");
      count = 1;
      for (const auto &i : m_adjncy_blocks) {
        str_msg_log.append(fmt::format("{} : ", count++));
        for (const auto &j : i) {
          str_msg_log.append(fmt::format("{}", j + 1));
          if (&(j) != &(*i.rbegin()))
            str_msg_log.append(", ");
        }
        str_msg_log.append("\n");
      }
      str_msg_log.append("Checking for cycles ...\n");
      str_msg_log.append(is_node_graph_cyclic
                             ? std::string("found cycles !\n")
                             : std::string("no cycles found\n"));
      if (!is_node_graph_cyclic) {
        str_msg_log.append("Performing topological sort ...\n");
        for (const auto &i : m_block_sequence) {
          str_msg_log.append(fmt::format("{}", i + 1));
          if (i != m_block_sequence.back())
            str_msg_log.append(" -> ");
        }
        if (m_block_sequence.front() + 1 != 1)
          str_msg_log.append(
              "\nError: First node is not == 1 !\nCheck your GNN\n");
        auto str_pair = (m_language == code_gen::C_CUDA_C)
                        ? code_gen<code_gen::C_CUDA_C>()
                        : code_gen<code_gen::C_Fortran>();
        m_inference_source = str_pair.first;
        m_inference_source_cplx = str_pair.second;
      }
      str_msg_log.append("\n\nPin features\n");
      for (const auto &item : m_pin_info.m_pinid_to_type) {
        str_msg_log.append(fmt::format("Pin #{} feature is {}\n", item.first,
                                       C_STR_FEATURE_TYPE[item.second + 1]));
      }
      message.SetText(str_msg_log);
    } catch (const std::exception &e) {
      message.SetText(e.what());
      clear_dynamic_data();
    }
  }

  /**
   * @brief
   *
   */
  void render() {
    int in_pin_counter = 1;
    int out_pin_counter = -1;
    int node_counter = 1;
    // Render graph-source block
    m_graph_source.render(node_counter++, in_pin_counter, out_pin_counter);
    out_pin_counter -= m_graph_source.m_edge_var_name.size();
    out_pin_counter -= m_graph_source.m_node_var_name.size();
    // Render encoder blocks
    for (auto &item : m_encoders)
      item.render(node_counter++, in_pin_counter++, out_pin_counter--);
    // Render loss block
    m_loss_function.render(node_counter++, in_pin_counter++, out_pin_counter);
    // Render the links to blocks
    int link_count = 0;
    for (const auto &my_pin1 : m_links)
      for (const auto &my_pin2 : my_pin1.second)
        ImNodes::Link(link_count++, my_pin1.first, my_pin2);
  }

  /**
   * @brief
   *
   * @return std::string
   */
  template<code_gen::CodeLanguage T_Language=code_gen::C_CUDA_C>
  std::pair<std::string, std::string> code_gen() {
    if (m_ordering == code_gen::C_ColumnMajorOrdering) {
      if (m_real_type == 1)
        return std::make_pair(
            code_gen_<T_Language, code_gen::C_ColumnMajorOrdering, double>(),
            code_gen_<T_Language, code_gen::C_ColumnMajorOrdering,
                      std::complex<double>>());
      else
        return std::make_pair(
            code_gen_<T_Language, code_gen::C_ColumnMajorOrdering, float>(),
            code_gen_<T_Language, code_gen::C_ColumnMajorOrdering,
                      std::complex<float>>());
    } else if (m_ordering == code_gen::C_RowMajorOrdering) {
      if (m_real_type == 1)
        return std::make_pair(
            code_gen_<T_Language, code_gen::C_RowMajorOrdering, double>(),
            code_gen_<T_Language, code_gen::C_RowMajorOrdering,
                      std::complex<double>>());
      else
        return std::make_pair(
            code_gen_<T_Language, code_gen::C_RowMajorOrdering, float>(),
            code_gen_<T_Language, code_gen::C_RowMajorOrdering,
                      std::complex<float>>());
    }
    return std::make_pair("Error: No code\n", "Error: No code\n");
  }

  /**
   * @brief
   *
   * @tparam T_Language
   * @tparam T_Ordering
   * @tparam T_Real
   * @return std::string
   */
  template <code_gen::CodeLanguage T_Language = code_gen::C_CUDA_C,
            code_gen::StorageOrdering T_Ordering = code_gen::C_RowMajorOrdering,
            typename T_Real = float>
  std::string code_gen_() {

    typedef code_gen::encoder_function<T_Language, T_Ordering, T_Real,
                                       EncoderNode, PinInfo>
        code_gen_encoder;
    typedef code_gen::function<T_Language, void> code_gen_function;
    typedef code_gen::scalar_variable<T_Language, int> code_gen_iscalar;
    typedef code_gen::array_variable<T_Language, T_Ordering, int, 2>
        code_gen_iarray2;
    typedef code_gen::array_variable<T_Language, T_Ordering, T_Real, 2>
        code_gen_darray2;
    typedef code_gen::array_variable<T_Language, T_Ordering, T_Real, 1>
        code_gen_darray1;

    std::string str_code;
    // Full network runner
    code_gen_function runner("run_network");
    // Add the graph inputs as arguements
    int m_nin = 0, m_nout = 0;
    runner.m_in[m_nin++] = new code_gen_iscalar("nnodes");
    runner.m_in[m_nin++] = new code_gen_iscalar("nedges");
    runner.m_in[m_nin++] = new code_gen_iarray2("edgelist", {"nedges", "2"});
    // Add the network parameter array
    unsigned param_size = 0;
    for (unsigned i = 0; i < m_encoders.size(); ++i) {
      auto enc = m_encoders.begin();
      std::advance(enc, i);
      param_size += enc->param_size;
    }
    runner.m_in[m_nin++] =
        new code_gen_darray1("params", {fmt::format("{}", param_size)});
    // Add the graph node and edge features
    for (unsigned ivar = 0; ivar < m_graph_source.m_edge_var_dim.size(); ++ivar)
      runner.m_in[m_nin++] = new code_gen_darray2(
          m_graph_source.m_edge_var_name[ivar].data(),
          {"nedges", fmt::format("{}", m_graph_source.m_edge_var_dim[ivar])});
    for (unsigned ivar = 0; ivar < m_graph_source.m_node_var_dim.size(); ++ivar)
      runner.m_in[m_nin++] = new code_gen_darray2(
          m_graph_source.m_node_var_name[ivar].data(),
          {"nnodes", fmt::format("{}", m_graph_source.m_node_var_dim[ivar])});
    std::vector<std::unique_ptr<code_gen_encoder>> enc_ptr(m_encoders.size());
    for (unsigned i = 0; i < m_encoders.size(); ++i) {
      auto enc = m_encoders.begin();
      std::advance(enc, i);
      enc_ptr[i].reset(new code_gen_encoder(enc->encoder_name.data(), *enc,
                                            this->m_pin_info));
      if (i == 0)
        str_code = enc_ptr[i]->generate_helper();
      enc_ptr[i]->setup_vars(m_nodeid_pinids[i + 1]);
      str_code.append(enc_ptr[i]->generate_mlp_code());
      runner.m_out[m_nout++] =
          new code_gen_darray2(enc_ptr[i]->get_output_name().c_str(),
                               {enc_ptr[i]->get_output_dim().c_str(),
                                fmt::format("{}", enc->encoder_dim.back())});
    }
    str_code.append("\n");

    //////////////////////////////////////////////
    ///////////// BEGIN RUN NETWORK //////////////
    //////////////////////////////////////////////
    str_code.append(runner.begin());

    // DATA COPY
    bool is_first = true;
    if(T_Language == code_gen::C_CUDA_C)
      str_code.append("\n  #pragma acc data copyin(");
    else if(T_Language == code_gen::C_Fortran)
      str_code.append("\n  !$acc data copyin(");
    // Data clause to copy data from host to device
    for (const auto &item : runner.m_in) {
      if (item.second->is_compound()) {
        if (!is_first)
          str_code.append(", ");
        else
          is_first = false;
        str_code.append(item.second->get_acc_data_declaration());
      }
    }
    str_code.append(") ");

    str_code.append("copyout(");
    is_first = true;
    for (const auto &item : runner.m_out) {
      if (item.second->is_compound()) {
        if (!is_first)
          str_code.append(", ");
        else
          is_first = false;
        str_code.append(item.second->get_acc_data_declaration());
      }
    }
    for (const auto &item : runner.m_inout) {
      if (item.second->is_compound()) {
        if (!is_first)
          str_code.append(", ");
        else
          is_first = false;
        str_code.append(item.second->get_acc_data_declaration() + " ");
      }
    }
    str_code.append(")\n");

    if(T_Language == code_gen::C_CUDA_C)
      str_code.append("  {  \n");

    int param_ofs = 0;
    for (unsigned i = 0; i < m_encoders.size(); ++i) {
      auto enc = m_encoders.begin();
      std::advance(enc, i);
      // Find and replace params by params + ofs
      auto str_fun = enc_ptr[i]->call();
      if (T_Language == code_gen::C_CUDA_C)
        code_gen::replace_all(str_fun, "params",
                              fmt::format("params + {}", param_ofs));
      else if (T_Language == code_gen::C_Fortran)
        code_gen::replace_all(str_fun, "params",
                              fmt::format("params({}:{})", param_ofs + 1,
                                          param_ofs + enc->param_size));
      str_code.append(str_fun);
      param_ofs += enc->param_size;
    }

    if(T_Language == code_gen::C_CUDA_C)
      str_code.append("  }\n" + runner.end());
    else
      str_code.append("!$acc end data\n" + runner.end());

    return str_code;
  }

  /**
   * @brief Get the inference filename object
   *
   * @return std::string
   */
  std::string get_inference_filename() {
    auto file_ext = (m_language == code_gen::C_Fortran) ? "f90" : "c";
#ifndef __EMSCRIPTEN__
    const auto generate_file_name = "Network/inference.build";
#else
    const auto generate_file_name = "inference.build";
#endif
    return fmt::format("{0}.{1}", generate_file_name, file_ext);
  }

  /**
   * @brief Get the inference filename cplx object
   *
   * @return std::string
   */
  std::string get_inference_filename_cplx() {
    auto file_ext = (m_language == code_gen::C_Fortran) ? "f90" : "c";
#ifndef __EMSCRIPTEN__
    const auto generate_file_name = "Network/inference.build";
#else
    const auto generate_file_name = "inference.build";
#endif
    return fmt::format("{0}.cplx.{1}", generate_file_name, file_ext);
  }

  std::string get_todiff_filename() {
    auto file_ext = (m_language == code_gen::C_Fortran) ? "f90" : "c";
#ifndef __EMSCRIPTEN__
    const auto generate_file_name = "Network/inference.build";
#else
    const auto generate_file_name = "inference.diff";
#endif
    return fmt::format("{0}.{1}", generate_file_name, file_ext);
  }

  /**
   * @brief
   *
   * @tparam T_Message
   * @tparam T_Editor
   * @param message
   * @param inference
   * @param backprop
   */
  template <typename T_Message, typename T_Editor>
  void build_library(T_Message &message, T_Editor &inference,
                     T_Editor &backprop) {
#ifndef __EMSCRIPTEN__
    if (!fs::exists("Network"))
      fs::create_directories("Network");
#endif
    std::ofstream fout(get_inference_filename());
    fout << inference.GetText();
    fout.close();
    fout.open(get_inference_filename_cplx());
    auto str_clean = m_inference_source_cplx;
    if(m_language == code_gen::C_CUDA_C) {
      code_gen::replace_all(str_clean, "//$AD II-LOOP ", "#");
      str_clean = "#include<math.h>\n#include<complex.h>\n\n";
    }
    fout << str_clean;
    fout.close();
    str_clean.clear();
#ifndef __EMSCRIPTEN__
    // Try to compile the C file into a shared lib
    auto str_msg = message.GetText();
    message.SetText("");
    str_msg.append("=============================================\n"
                   "  Building inference dynamic shared library\n"
                   "=============================================\n\n");
    TinyProcessLib::Process build_obj(
        "gcc -c -fPIC Network/inference.build.c -o "
        "Network/inference.build.o",
        "", [&](const char *bytes, size_t n) { str_msg.append(bytes, n); });
    if (build_obj.get_exit_status() == 0)
      str_msg.append("Successfully created object file\n");
    else
      str_msg.append("Error compiling inference to object file\n");

    TinyProcessLib::Process build_so(
        "gcc -fPIC -shared Network/inference.build.o -o "
        "Network/libInference.so",
        "", [&](const char *bytes, size_t n) { str_msg.append(bytes, n); });
    if (build_so.get_exit_status() == 0)
      str_msg.append("Successfully created shared library - libInference.so\n");
    else
      str_msg.append("Error compiling shared library\n");
    message.SetText(str_msg);
#endif
  }

  /**
   * @brief
   *
   * @tparam T_Message
   * @tparam T_Editor
   * @param message
   * @param inference
   * @param backprop
   */
  template <typename T_Message, typename T_Editor>
  void auto_diff(T_Message &message, T_Editor &inference, T_Editor &backprop) {
    // Build the inference code
    build_library(message, inference, backprop);
    // Run tapenade to get the derivative code
    std::ofstream fout(get_todiff_filename());
    fout << m_inference_source;
    fout.close();
#ifndef __EMSCRIPTEN__
    // Try to compile the C file into a shared lib
    auto str_msg = message.GetText();
    message.SetText("");
    str_msg.append("=============================================\n"
                   "      AutoDiff inference using Tapeande       \n"
                   "=============================================\n\n");
    // Build the list of variables connected to loss function
    // 1. Loop over all the input pins of loss function
    // 2. Loop over the output pins associated with the input pins
    // 3. Add the pinout variable name to a list
    std::string cmd_tapenade = fmt::format("tapenade -b {} -O "
                               "Network -head \"run_network(params)\\(", get_todiff_filename());
    for (auto &item : m_nodeid_pinids.back()) {
      if (item > 0) {
        auto found_in_to_outs = m_pin_info.m_pin_in_to_outs.find(item);
        assert(found_in_to_outs != std::end(m_pin_info.m_pin_in_to_outs));
        for (auto &pinin : found_in_to_outs->second) {
          auto found_type = m_pin_info.m_pinid_to_type.find(pinin);
          assert(found_type != std::end(m_pin_info.m_pinid_to_type));
          auto found_name = m_pin_info.m_pinid_to_name.find(pinin);
          cmd_tapenade += found_name->second;
          cmd_tapenade += ", ";
        }
      }
    }
    cmd_tapenade.pop_back(); // " "
    cmd_tapenade.pop_back(); // ","
    cmd_tapenade += ")\"";
    str_msg += "Tapenade was run using command\n" + cmd_tapenade + "\n\n";
    TinyProcessLib::Process build_obj(
        cmd_tapenade, "",
        [&](const char *bytes, size_t n) { str_msg.append(bytes, n); });
    if (build_obj.get_exit_status() == 0)
      str_msg.append("AutoDiff successful\n");
    else
      str_msg.append("Error in AutoDiff\n");
    message.SetText(str_msg);
    std::ifstream fin("Network/inference.diff_b.msg");
    if (!fin.fail()) {
      fin.seekg(0, std::ios::end);
      size_t size = fin.tellg();
      std::string buffer(size, ' ');
      fin.seekg(0);
      fin.read(&buffer[0], size);
      str_msg += buffer;
      message.SetText(str_msg);
    }

    fin.close();
    fin.open("Network/inference.diff_b.c");
    if (!fin.fail()) {
      fin.seekg(0, std::ios::end);
      size_t size = fin.tellg();
      std::string buffer(size, ' ');
      fin.seekg(0);
      fin.read(&buffer[0], size);
      code_gen::replace_all(buffer, "$BWD-OF II-LOOP ", "#");
      code_gen::replace_all(buffer, "$FWD-OF II-LOOP ", "#");
      code_gen::replace_all(buffer, "$AD II-LOOP ", "#");
      code_gen::replace_all(buffer, "//#pragma acc ", "#pragma acc ");
      code_gen::replace_all(buffer, "#include <adStack.h>",
                            "//#include <adStack.h>\n#include <math.h>");
      backprop.SetText(buffer);
      std::ofstream fout_diff("Network/inference_b.c");
      fout_diff << buffer;
      fout_diff.close();
    }
#else
    message.SetText("Error: Feature not available in Emscripten mode\n");
#endif
  }

  /**
   * @brief
   *
   */
  void clear_dynamic_data() {
    // Node parameters that must be dynamically determined
    m_adjncy_blocks.clear();
    m_adjncy_blocks.shrink_to_fit();
    /// @brief
    m_block_sequence.clear();
    m_block_sequence.shrink_to_fit();
    /// @brief
    m_nodeid_pinids.clear();
    m_nodeid_pinids.shrink_to_fit();
    /// @brief
    m_node_feat_type.clear();
    m_node_feat_type.shrink_to_fit();

    m_pin_info.clear();
  }

  GraphDataSource m_graph_source;
  std::list<EncoderNode> m_encoders;
  LossFunction m_loss_function;
  std::map<int, std::set<int>> m_links;

  // Node parameters that must be dynamically determined
  std::vector<std::set<unsigned>> m_adjncy_blocks;
  std::vector<unsigned> m_block_sequence;
  std::vector<std::set<int>> m_nodeid_pinids;
  std::vector<feature_types_t> m_node_feat_type;
  std::string m_inference_source;
  std::string m_inference_source_cplx;
  PinInfo m_pin_info;
  code_gen::StorageOrdering m_ordering = code_gen::C_RowMajorOrdering;
  int m_real_type = 0;
  code_gen::CodeLanguage m_language;
};

void to_json(json& j, const GraphNetworkModel& gnn) {
  j = json{
    {"m_graph_source", gnn.m_graph_source},
    {"m_encoders", gnn.m_encoders},
    {"m_loss_function", gnn.m_loss_function},
    {"m_links", gnn.m_links},

    {"m_adjncy_blocks", gnn.m_adjncy_blocks},
    {"m_block_sequence", gnn.m_block_sequence},
    {"m_nodeid_pinids", gnn.m_nodeid_pinids},
    {"m_node_feat_type", gnn.m_node_feat_type},
    {"m_inference_source", gnn.m_inference_source},
    {"m_inference_source_cplx", gnn.m_inference_source_cplx},
    {"m_pin_info", gnn.m_pin_info},
    {"m_ordering", gnn.m_ordering},
    {"m_real_type", gnn.m_real_type}
  };
}

void from_json(const json& j, GraphNetworkModel& gnn) {
  j.at("m_graph_source").get_to(gnn.m_graph_source);
  j.at("m_encoders").get_to(gnn.m_encoders);
  j.at("m_loss_function").get_to(gnn.m_loss_function);
  j.at("m_links").get_to(gnn.m_links);

  j.at("m_adjncy_blocks").get_to(gnn.m_adjncy_blocks);
  j.at("m_block_sequence").get_to(gnn.m_block_sequence);
  j.at("m_nodeid_pinids").get_to(gnn.m_nodeid_pinids);
  j.at("m_node_feat_type").get_to(gnn.m_node_feat_type);
  j.at("m_inference_source").get_to(gnn.m_inference_source);
  j.at("m_inference_source_cplx").get_to(gnn.m_inference_source_cplx);
  j.at("m_pin_info").get_to(gnn.m_pin_info);
  j.at("m_ordering").get_to(gnn.m_ordering);
  j.at("m_real_type").get_to(gnn.m_real_type);
}

static GraphNetworkModel g_gnn;
static TextEditor editor_primal;
static TextEditor editor_adjoint;
static TextEditor editor_message;

/**
 * @brief
 *
 */
void BuildOptions() {
  static bool show_code_editor = false;
  if (ImGui::IsKeyDown(ImGuiKey_LeftCtrl))
    if (ImGui::IsKeyPressed(ImGuiKey_O))
      show_code_editor = show_code_editor ^ 1;

  if (show_code_editor) {
    ImGui::SetNextWindowSize(ImVec2(500, 440), ImGuiCond_FirstUseEver);
    ImGui::Begin(ICON_FA_WRENCH " Build Options");
    ImGui::End();
  }
}

/**
 * @brief
 *
 */
void AutoDiffOptions() {
  static bool show_code_editor = false;

  if (show_code_editor) {
    ImGuiID dockspace_id = ImGui::GetID("MyDockSpace");
    ImGui::SetNextWindowDockID(dockspace_id);
    ImGui::SetNextWindowSize(ImVec2(500, 440), ImGuiCond_FirstUseEver);
    ImGui::Begin(ICON_FA_CALCULATOR " AutoDiff Options");
    ImGui::Spacing();
    ImGui::Spacing();
    ImGui::Text("   Autodiff powered by Tapenade " ICON_FA_COPYRIGHT);
    ImGui::Text("  ");
    ImGui::SameLine();
    ImGui::Image((void *)(intptr_t)inria_logo_texture,
                 ImVec2(inria::logo_w, inria::logo_h));
    ImGui::SameLine();
    ImGui::Image((void *)(intptr_t)tapenade_logo_texture,
                 ImVec2(1.2 * tapenade::logo_w, 1.2 * tapenade::logo_h));
    ImGui::SameLine();
    ImGui::Image((void *)(intptr_t)cerfacs_logo_texture,
                 ImVec2(0.7 * cerfacs::logo_w, 0.7 * cerfacs::logo_h));
    ImGui::SameLine();
    ImGui::Image((void *)(intptr_t)avatar_logo_texture,
                 ImVec2(0.4 * avatar::logo_w, 0.4 * avatar::logo_h));
    ImGui::End();
  }
}

/**
 * @brief
 *
 */
void Help() {
  ImGui::SetNextWindowSize(ImVec2(500, 440), ImGuiCond_FirstUseEver);
  ImGuiID dockspace_id = ImGui::GetID("MyDockSpace");
  ImGui::SetNextWindowDockID(dockspace_id);
  ImGui::Begin(ICON_FA_CALCULATOR " About");
  ImGui::Spacing();
  ImGui::Spacing();
  ImGui::Text(ICON_FA_CHILD_DRESS ICON_FA_CHILD_DRESS ICON_FA_CHILD_DRESS
                  ICON_FA_CHILD_DRESS ICON_FA_CHILD_DRESS ICON_FA_CHILD_DRESS
                      ICON_FA_CHILD_DRESS ICON_FA_CHILD_DRESS
              " Anamika DSL " ICON_FA_CHILD_DRESS ICON_FA_CHILD_DRESS
                  ICON_FA_CHILD_DRESS ICON_FA_CHILD_DRESS ICON_FA_CHILD_DRESS
                      ICON_FA_CHILD_DRESS ICON_FA_CHILD_DRESS);
  ImGui::Image((void *)(intptr_t)anamika_logo_texture,
               ImVec2(0.75 * anamika::logo_w, 0.75 * anamika::logo_h));
  ImGui::Text("GUI/CodeGen developed at CERFACS " ICON_FA_COPYRIGHT);
  ImGui::Image((void *)(intptr_t)cerfacs_logo_texture,
               ImVec2(0.6 * cerfacs::logo_w, 0.6 * cerfacs::logo_h));
  ImGui::Text("   Autodiff powered by Tapenade " ICON_FA_COPYRIGHT);
  ImGui::Text("  ");
  ImGui::SameLine();
  ImGui::Image((void *)(intptr_t)inria_logo_texture,
               ImVec2(inria::logo_w, inria::logo_h));
  ImGui::SameLine();
  ImGui::Image((void *)(intptr_t)tapenade_logo_texture,
               ImVec2(1.2 * tapenade::logo_w, 1.2 * tapenade::logo_h));
  ImGui::Text("Creator/Maintainer \nPavanakumar Mohanamuraly \nmpkumar at "
              "cerfacs dot fr");
  ImGui::Image((void *)(intptr_t)avatar_logo_texture,
               ImVec2(0.4 * avatar::logo_w, 0.4 * avatar::logo_h));
  ImGui::Text("For documentation refer to URL ***");
  ImGui::End();
}

/**
 * @brief Show menu
 *
 */
void CodeEditor(ImGuiID &dockspace_id) {
  editor_message.SetReadOnly(true);

  // static bool opt_fullscreen = true;
  // static bool opt_padding = false;

  //////////////////////////////
  /// @brief Message Logger ////
  //////////////////////////////
  SDL_DisplayMode DM;
  SDL_GetCurrentDisplayMode(0, &DM);
  auto Width = DM.w - 0.1 * DM.w;
  auto Height = DM.h - 0.2 * DM.h;
  ImGui::SetNextWindowDockID(dockspace_id);
  ImGui::SetNextWindowSize(ImVec2(Width, Height), ImGuiCond_FirstUseEver);
  ImGui::Begin(" Message Log", nullptr,
               ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_MenuBar);
  ImGui::BeginMenuBar();
  if (ImGui::BeginMenu("Edit##MessageLog")) {
    if (ImGui::MenuItem("Copy##MessageLog", "", nullptr,
                        editor_message.HasSelection())) {
      editor_message.Copy();
    }
    if (ImGui::MenuItem("Clear##MessageLog", "")) {
      editor_message.SetText("");
    }
    ImGui::EndMenu();
  }
  ImGui::EndMenuBar();
  editor_message.Render("CodeEditorMessageLog");
  ImGui::End();

  //////////////////////////////
  /// @brief Adjoint editor ////
  //////////////////////////////
  ImGui::SetNextWindowDockID(dockspace_id);
  ImGui::SetNextWindowSize(ImVec2(Width, Height), ImGuiCond_FirstUseEver);
  ImGui::Begin(" AutoDiff", nullptr,
               ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_MenuBar);
  ImGui::BeginMenuBar();
  if (ImGui::BeginMenu("Edit##Adjoint")) {
    if (ImGui::MenuItem("Undo##Adjoint", "", nullptr,
                        editor_adjoint.HasSelection())) {
      editor_adjoint.Undo();
    }
    if (ImGui::MenuItem("Redo##Adjoint", "", nullptr,
                        editor_adjoint.HasSelection())) {
      editor_adjoint.Redo();
    } // Disabled item
    if (ImGui::MenuItem("Cut##Adjoint", "", nullptr,
                        editor_adjoint.HasSelection())) {
      editor_adjoint.Cut();
    }
    if (ImGui::MenuItem("Copy##Adjoint", "", nullptr,
                        editor_adjoint.HasSelection())) {
      editor_adjoint.Copy();
    }
    if (ImGui::MenuItem("Paste##Adjoint", "")) {
      editor_adjoint.Paste();
    }
    ImGui::EndMenu();
  }
  ImGui::EndMenuBar();
  editor_adjoint.Render("CodeEditorAdjoint");
  ImGui::End();

  //////////////////////////////
  /// @brief Primal editor ////
  //////////////////////////////
  ImGui::SetNextWindowDockID(dockspace_id);
  ImGui::SetNextWindowSize(ImVec2(Width, Height), ImGuiCond_FirstUseEver);
  ImGui::Begin("  Code Generator", nullptr,
               ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_MenuBar);
  ImGui::BeginMenuBar();
  if (ImGui::BeginMenu("File##Primal")) {
    if (ImGui::MenuItem("AutoDiff", "")) {
      g_gnn.auto_diff(editor_message, editor_primal, editor_adjoint);
#ifdef __EMSCRIPTEN__
      emscripten::val::global("window").call<void>("offerFileAsDownload", std::string("inference_cplx.build.c"), std::string("mime/type"));
#endif
    }
    if (ImGui::MenuItem("Build", "")) {
      g_gnn.build_library(editor_message, editor_primal, editor_adjoint);
#ifdef __EMSCRIPTEN__
      emscripten::val::global("window").call<void>("offerFileAsDownload", std::string("inference.build.c"), std::string("mime/type"));
#endif
    }
    if (ImGui::MenuItem("(Re)Generate", "")) {
      g_gnn.generate_code(editor_message);
      auto str_clean = g_gnn.m_inference_source;
      if(g_gnn.m_language == code_gen::C_CUDA_C) {
        code_gen::replace_all(str_clean, "//$AD II-LOOP ", "#");
        str_clean = "#include<math.h>\n\n" + str_clean;
      }
      editor_primal.SetText(str_clean);
    }
    ImGui::EndMenu();
  }
  if (ImGui::BeginMenu("Edit##Primal")) {
    if (ImGui::MenuItem("Undo##Primal", "", nullptr,
                        editor_primal.HasSelection())) {
      editor_primal.Undo();
    }
    if (ImGui::MenuItem("Redo##Primal", "", nullptr,
                        editor_primal.HasSelection())) {
      editor_primal.Redo();
    } // Disabled item
    if (ImGui::MenuItem("Cut##Primal", "", nullptr,
                        editor_primal.HasSelection())) {
      editor_primal.Cut();
    }
    if (ImGui::MenuItem("Copy##Primal", "", nullptr,
                        editor_primal.HasSelection())) {
      editor_primal.Copy();
    }
    if (ImGui::MenuItem("Paste##Primal", "")) {
      editor_primal.Paste();
    }
    ImGui::EndMenu();
  }

  // Language selection
  if (ImGui::RadioButton("f90##code-gen-f90", (int *)&(g_gnn.m_language), (int)(code_gen::C_Fortran))) {}
  if (ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenDisabled))
    ImGui::SetTooltip("Fortran90 CodeGen");

  if (ImGui::RadioButton("c##code-gen-c", (int *)&(g_gnn.m_language), (int)(code_gen::C_CUDA_C))) {}
  if (ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenDisabled))
    ImGui::SetTooltip("C99 CodeGen");

  // Storage ordering selection
  if (ImGui::RadioButton(ICON_FA_BARS, (int *)&(g_gnn.m_ordering), (int)(code_gen::C_RowMajorOrdering))) {}
  if (ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenDisabled))
    ImGui::SetTooltip("Row-Major");

  if (ImGui::RadioButton(ICON_FA_BARCODE, (int *)&(g_gnn.m_ordering), (int)(code_gen::C_ColumnMajorOrdering))) {}
  if (ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenDisabled))
    ImGui::SetTooltip("Column-Major");

  ImGui::EndMenuBar();
  editor_primal.Render("CodeEditorPrimal");
  ImGui::End();
}

/**
 * @brief
 *
 */
void ShowDemoWindow(bool *) {
  ImGuiID dockspace_id = ImGui::GetID("MyDockSpace");

  // Register the code editor
  CodeEditor(dockspace_id);
  SDL_DisplayMode DM;
  SDL_GetCurrentDisplayMode(0, &DM);
  auto Width = DM.w - 0.1 * DM.w;
  auto Height = DM.h - 0.2 * DM.h;
  ImGui::SetNextWindowDockID(dockspace_id);
  ImGui::SetNextWindowSize(ImVec2(Width, Height), ImGuiCond_FirstUseEver);
  ImGui::Begin(" Network modeller", nullptr,
               ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_MenuBar);
  ImGui::BeginMenuBar();
  if (ImGui::BeginMenu("File")) {
    if (ImGui::MenuItem("PushBack Encoder")) {
      g_gnn.m_encoders.push_back(EncoderNode());
    }
    if (ImGui::MenuItem("PopBack Encoder")) {
      g_gnn.m_encoders.pop_back();
    }
    if (ImGui::MenuItem(ICON_FA_ARROWS_ROTATE " Refresh")) {
      try {
        g_gnn.remap_pinid_to_nodeid();
        auto is_node_graph_cyclic = g_gnn.create_node_graph();
        if (!is_node_graph_cyclic) {
          g_gnn.remap_pinid_to_type(editor_message);
          g_gnn.recalc_param_sizes(editor_message);
        }
      } catch (const std::exception &e) {
        editor_message.SetText(e.what());
        g_gnn.clear_dynamic_data();
      }
    }
    ImGui::EndMenu();
  }
  if (ImGui::BeginMenu(ICON_FA_CIRCLE_QUESTION)) {
    ImGui::SetNextWindowSize(ImVec2(500, 440), ImGuiCond_FirstUseEver);
    Help();
    ImGui::EndMenu();
  }

  // Download/save model
  if (ImGui::Button(ICON_FA_DOWNLOAD)) {
    std::ofstream fout("model.json");
    json j = g_gnn;
    fout << j.dump() << "\n";
    fout.close();
#ifdef __EMSCRIPTEN__
    emscripten::val::global("window").call<void>("offerFileAsDownload", std::string("model.json"), std::string("mime/type"));
#endif
  }
  if (ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenDisabled))
    ImGui::SetTooltip("Save model");

  // Load model
  if(ImGui::Button(ICON_FA_UPLOAD))
    ImGuiFileDialog::Instance()->OpenDialog("ChooseModelJSONFileDlgKey",
                                            "Choose Model", ".json", ".");
  if (ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenDisabled))
    ImGui::SetTooltip("Load model");
  // Display
  ImGui::SetNextWindowSize(ImVec2(500, 440), ImGuiCond_FirstUseEver);
  if (ImGuiFileDialog::Instance()->Display("ChooseModelJSONFileDlgKey")) {
    // Action if OK
    if (ImGuiFileDialog::Instance()->IsOk()) {
      std::string model_file_name =
          ImGuiFileDialog::Instance()->GetFilePathName();
      std::string model_file_path =
          ImGuiFileDialog::Instance()->GetCurrentPath();
      // Action
      // std::cout << "Model File Name : " << model_file_name << "\n";
      // std::cout << "Model File Path : " << model_file_path << "\n";
      std::ifstream f(model_file_name);
      json data = json::parse(f);
      g_gnn = data.get<GraphNetworkModel>();
    }
    ImGui::SameLine();
    // Close
    ImGuiFileDialog::Instance()->Close();
  }
  ImGui::EndMenuBar();
  ImNodes::BeginNodeEditor();

  if (ImGui::IsKeyDown(ImGuiKey_LeftCtrl))
    if (ImGui::IsKeyPressed(ImGuiKey_E))
      g_gnn.m_encoders.push_back(EncoderNode());

  // Render the Graph network
  g_gnn.render();

  // Register the AutoDiff options
  AutoDiffOptions();

  // Register build options
  BuildOptions();

  // Close the ImNodes editor
  ImNodes::MiniMap(0.15f, ImNodesMiniMapLocation_BottomLeft);
  ImNodes::EndNodeEditor();

  /*
      ImNode editor is closed here we register the callbacks for
      deleting the links connecting the blocks and the encoders.
      This still needs work as deleting encoders screws up the
      links ...
  */

  int start_attr, end_attr;
  if (ImNodes::IsLinkCreated(&start_attr, &end_attr))
    g_gnn.m_links[std::min(start_attr, end_attr)].insert(
        std::max(start_attr, end_attr));

  int link_count = 0;
  for (auto i = g_gnn.m_links.begin(); i != g_gnn.m_links.end(); ++i) {
    if (ImNodes::IsLinkSelected(link_count) &&
        ImGui::IsKeyPressed(ImGuiKey_X)) {
      i = g_gnn.m_links.erase(i);
      break;
    }
    link_count++;
  }
  int dummy_count = 2;
  for (auto item = g_gnn.m_encoders.begin(); item != g_gnn.m_encoders.end();
       ++item) {
    if (ImNodes::IsNodeSelected(dummy_count) &&
        ImGui::IsKeyPressed(ImGuiKey_X)) {
      item = g_gnn.m_encoders.erase(item);
      g_gnn.m_links.clear();
      break;
    }
    dummy_count++;
  }
  ImGui::End();
}

} // namespace ImGui
