#pragma once

#include <emscripten.h>
#include <emscripten/bind.h>
#include <string>

/**
 * @brief Function sends string to web server using
 *        emscripten API given url string
 *
 * @param input_str string to send to web server
 * @param url       url string to send string to
 */
void send_string(const std::string &input_str, const std::string &url) {
  EM_ASM(
    var input_str = UTF8ToString($0);
    var url = UTF8ToString($1);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", url, false);
    xhr.setRequestHeader("Content-Type", "text/plain");
    xhr.send(input_str);
  , input_str.c_str(), url.c_str());
}

/**
 * @brief Get the string object from web server
 *        using emscripten API given url string
 *
 * @param url           url string to get string from
 * @return std::string  string from web server
 */
std::string get_string(const std::string &url) {
  std::string output_str;
  EM_ASM(
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url.c_str(), false);
    xhr.send(null);
    var output_str = xhr.responseText;
    var lengthBytes = lengthBytesUTF8(output_str)+1;
    var stringOnWasmHeap = _malloc(lengthBytes);
    stringToUTF8(output_str, stringOnWasmHeap, lengthBytes);
    Module['stringOnWasmHeap'] = stringOnWasmHeap;
  );
  output_str = (char*)EM_ASM_INT({
    var stringOnWasmHeap = Module['stringOnWasmHeap'];
    var output_str = UTF8ToString(stringOnWasmHeap);
    _free(stringOnWasmHeap);
    return output_str;
  });
  return output_str;
}
