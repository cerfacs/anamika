#pragma once

#include <algorithm>
#include <iostream>
#include <set>
#include <vector>

/**
 * @brief
 *
 * @tparam T_AdjArray
 * @tparam T_BoolArray
 * @param v
 * @param adj
 * @param recStack
 * @param visited
 * @return true
 * @return false
 */
template <typename T_AdjArray, typename T_BoolArray>
bool is_cyclic_util(unsigned v, const T_AdjArray &adj, T_BoolArray &recStack,
                    T_BoolArray &visited) {
  if (visited[v] == false) {
    // Mark the current node as visited and part of recursion stack
    visited[v] = true;
    recStack[v] = true;

    // Recur for all the vertices adjacent to this vertex
    for (const auto &i : adj[v]) {
      if (!visited[i] && is_cyclic_util(i, adj, recStack, visited))
        return true;
      else if (recStack[i])
        return true;
    }
  }
  recStack[v] = false; // remove the vertex from recursion stack
  return false;
}

/**
 * @brief
 *
 * @tparam T_AdjArray
 * @tparam T_BoolArray
 * @param adj
 * @return true If graph has cycles
 * @return false If graph is cycle-free
 */
template <typename T_AdjArray> bool is_cyclic(const T_AdjArray &adj) {
  // Mark all the vertices as not visited and not part of recursion
  // stack
  std::vector<bool> visited(adj.size(), false);
  std::vector<bool> recStack(adj.size(), false);

  // Call the recursive helper function to detect cycle in different
  // DFS trees
  for (unsigned i = 0; i < adj.size(); i++)
    if (!visited[i] && is_cyclic_util(i, adj, recStack, visited))
      return true;
  return false;
}

/**
 * @brief
 *
 * @tparam T_IntArray
 * @tparam T_BoolArray
 * @tparam T_AdjArray
 * @param v
 * @param adj
 * @param visited
 * @param dfs
 */
template <typename T_IntArray, typename T_BoolArray, typename T_AdjArray>
void depth_first_search(const unsigned v, T_AdjArray &adj, T_BoolArray &visited,
                        T_IntArray &dfs) {
  visited[v] = true;
  for (unsigned u : adj[v]) {
    if (!visited[u])
      depth_first_search(u, adj, visited, dfs);
  }
  dfs.push_back(v);
}

/**
 * @brief
 *
 * @tparam T_AdjArray
 * @param adj
 * @return std::vector<unsigned>
 */
template <typename T_AdjArray>
std::vector<unsigned> topological_sort(T_AdjArray &adj) {
  std::vector<bool> visited(adj.size(), false);
  std::vector<unsigned> dfs;
  for (unsigned i = 0; i < adj.size(); ++i) {
    if (!visited[i])
      depth_first_search(i, adj, visited, dfs);
  }
  std::reverse(dfs.begin(), dfs.end());
  return dfs;
}

/**
 * @brief
 *
 * @return int
 */
int test_topological_sort_vv() {
  std::vector<std::vector<unsigned>> adj;
  std::vector<unsigned> result{0, 2, 1, 3, 4, 5};
#if 0
problem:
           |-> 1 -> 4 \
           0     \     5
           |-> 2 -> 3 /
result:
  0 -> 2 -> 1 -> 3 -> 4 -> 5
#endif
  adj.push_back(std::vector<unsigned>{1, 2}); // vertex 0
  adj.push_back(std::vector<unsigned>{4, 3}); // vertex 1
  adj.push_back(std::vector<unsigned>{3});    // vertex 2
  adj.push_back(std::vector<unsigned>{5});    // vertex 3
  adj.push_back(std::vector<unsigned>{5});    // vertex 4
  auto dfs = topological_sort(adj);
  unsigned count = 0;
  for (auto &item : dfs)
    if (item != result[count++])
      return -1;
#if DEBUG_PRINT
  for (auto &item : dfs) {
    std::cout << item;
    if (item != dfs.back())
      std::cout << " -> ";
  }
  std::cout << "\n";
#endif
  return 0;
}

/**
 * @brief
 *
 * @return int
 */
int test_topological_sort_vs() {
  std::vector<std::set<unsigned>> adj;
  std::vector<unsigned> result{0, 2, 1, 3, 4, 5};
#if 0
problem:
           |-> 1 -> 4 \
           0     \     5
           |-> 2 -> 3 /
result:
  0 -> 2 -> 1 -> 3 -> 4 -> 5
#endif
  adj.push_back(std::set<unsigned>{1, 2}); // vertex 0
  adj.push_back(std::set<unsigned>{4, 3}); // vertex 1
  adj.push_back(std::set<unsigned>{3});    // vertex 2
  adj.push_back(std::set<unsigned>{5});    // vertex 3
  adj.push_back(std::set<unsigned>{5});    // vertex 4
  auto dfs = topological_sort(adj);
  unsigned count = 0;
  for (auto &item : dfs)
    if (item != result[count++])
      return -1;
#if DEBUG_PRINT
  for (auto &item : dfs) {
    std::cout << item;
    if (item != dfs.back())
      std::cout << " -> ";
  }
  std::cout << "\n";
#endif
  return 0;
}
