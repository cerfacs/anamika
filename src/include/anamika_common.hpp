#pragma once

#include "json.hpp"
using json = nlohmann::json;

namespace code_gen {


/**
 * @brief
 *
 */
enum StorageOrdering { C_RowMajorOrdering = 0, C_ColumnMajorOrdering };

// map StorageOrdering values to JSON as strings
NLOHMANN_JSON_SERIALIZE_ENUM( StorageOrdering, {
    {C_RowMajorOrdering, "C_RowMajorOrdering"},
    {C_ColumnMajorOrdering, "C_ColumnMajorOrdering"}
})

/**
 * @brief
 *
 */
enum CodeLanguage { C_Fortran = 0, C_CUDA_C };

/**
 * @brief
 *
 */
enum IntentTypes { C_IntentNone = 0, C_IntentIn, C_IntentOut, C_IntentInOut };

/**
 * @brief Get the c index object
 *
 * @tparam T_Ordering
 * @param ijk
 * @return std::string
 */
template<StorageOrdering T_Ordering>
std::string get_c_index(const std::vector<std::string> ijk) {
  switch(T_Ordering) {
    case C_RowMajorOrdering:
    return fmt::format("[{}]", fmt::join(ijk.begin(), ijk.end(), "]["));
    break;

    case C_ColumnMajorOrdering:
    return fmt::format("[{}]", fmt::join(ijk.rbegin(), ijk.rend(), "]["));
    break;

  }
}

/**
 * @brief
 *
 * @param s
 * @param toReplace
 * @param replaceWith
 */
void replace_all(std::string &s, std::string const &toReplace,
                 std::string const &replaceWith) {
  std::string buf;
  std::size_t pos = 0;
  std::size_t prevPos;

  // Reserves rough estimate of final size of string.
  buf.reserve(s.size());

  while (true) {
    prevPos = pos;
    pos = s.find(toReplace, pos);
    if (pos == std::string::npos)
      break;
    buf.append(s, prevPos, pos - prevPos);
    buf += replaceWith;
    pos += toReplace.size();
  }

  buf.append(s, prevPos, s.size() - prevPos);
  s.swap(buf);
}

/**
 * @brief Get the intent string object
 *
 * @param itype
 * @return const std::string
 */
const std::string get_intent_string(const IntentTypes itype) {
  std::string s_ret;
  switch (itype) {
  case C_IntentIn:
    s_ret = ", intent(in)";
    break;

  case C_IntentOut:
    s_ret = ", intent(out)";
    break;

  case C_IntentInOut:
    s_ret = ", intent(inout)";
    break;

  default:
    break;
  }
  return s_ret;
}

/**
 * @brief Get the type string object
 *
 * @tparam T_Language
 * @tparam T
 * @return const std::string
 */
template <CodeLanguage T_Language, typename T>
const std::string get_type_string() {
  std::string s_ret;
  if (T_Language == C_Fortran) {
    if (typeid(T) == typeid(double))
      s_ret = "real(c_double)";
    else if (typeid(T) == typeid(float))
      s_ret = "real(c_float)";
    else if (typeid(T) == typeid(int))
      s_ret = "real(c_int32_t)";
    else if (typeid(T) == typeid(long int))
      s_ret = "real(c_int64_t)";
    else if (typeid(T) == typeid(std::complex<double>))
      s_ret = "complex(c_double_complex)";
    else if (typeid(T) == typeid(std::complex<float>))
      s_ret = "complex(c_float_complex)";
  } else if (T_Language == C_CUDA_C) {
    if (typeid(T) == typeid(double))
      s_ret = "double ";
    else if (typeid(T) == typeid(float))
      s_ret = "float ";
    else if (typeid(T) == typeid(int))
      s_ret = "int ";
    else if (typeid(T) == typeid(long int))
      s_ret = "long int ";
    else if (typeid(T) == typeid(std::complex<double>))
      s_ret = "double complex ";
    else if (typeid(T) == typeid(std::complex<float>))
      s_ret = "float complex ";
  }
  return s_ret;
}

}

/**
 * @brief
 *
 */
enum feature_types_t {
  C_FEAT_UNKNOWN = -1,
  C_FEAT_NODE = 0,
  C_FEAT_EDGE_ASYMMETRIC,
  C_FEAT_EDGE_SYMMETRIC,
  C_FEAT_MIXED_NODE_EDGE_ASYMMETRIC,
  C_FEAT_MIXED_NODE_EDGE_SYMMETRIC,
  C_FEAT_GLOBAL_NODE,
  C_FEAT_GLOBAL_EDGE
};

/**
 * @brief
 *
 */
const char *C_STR_FEATURE_TYPE[] = {"C_FEAT_UNKNOWN",
                                    "C_FEAT_NODE",
                                    "C_FEAT_EDGE_ASYMMETRIC",
                                    "C_FEAT_EDGE_SYMMETRIC",
                                    "C_FEAT_MIXED_NODE_EDGE_ASYMMETRIC",
                                    "C_FEAT_MIXED_NODE_EDGE_SYMMETRIC",
                                    "C_FEAT_GLOBAL_NODE",
                                    "C_FEAT_GLOBAL_EDGE"};

// map feature_types_t values to JSON as strings
NLOHMANN_JSON_SERIALIZE_ENUM( feature_types_t, {
    {C_FEAT_UNKNOWN, C_STR_FEATURE_TYPE[0]},
    {C_FEAT_NODE, C_STR_FEATURE_TYPE[1]},
    {C_FEAT_EDGE_ASYMMETRIC, C_STR_FEATURE_TYPE[2]},
    {C_FEAT_EDGE_SYMMETRIC, C_STR_FEATURE_TYPE[3]},
    {C_FEAT_MIXED_NODE_EDGE_ASYMMETRIC, C_STR_FEATURE_TYPE[4]},
    {C_FEAT_MIXED_NODE_EDGE_SYMMETRIC, C_STR_FEATURE_TYPE[5]},
    {C_FEAT_GLOBAL_NODE, C_STR_FEATURE_TYPE[6]},
    {C_FEAT_GLOBAL_EDGE, C_STR_FEATURE_TYPE[7]}})

/**
 * @brief
 *
 */
enum scatter_function_t {
  C_SYMMETRIC_ACCUMULATE = 0,
  C_ASYMMETRIC_ACCUMULATE,
  C_ASYMMETRIC_ACCUMULATE1
};

/**
 * @brief
 *
 */
const char *C_ScatterFunctionNames[] = {"++", "+-", "-+"};

NLOHMANN_JSON_SERIALIZE_ENUM( scatter_function_t, {
    {C_SYMMETRIC_ACCUMULATE, C_ScatterFunctionNames[0]},
    {C_ASYMMETRIC_ACCUMULATE, C_ScatterFunctionNames[1]},
    {C_ASYMMETRIC_ACCUMULATE1, C_ScatterFunctionNames[2]}
})

/**
 * @brief
 *
 */
enum ActivationFunctionType { C_Relu = 0, C_LeakyRelu = 1, C_Tanh = 2, C_Ident = 3 };

/**
 * @brief
 *
 */
const char *C_ActivationFunctionNames[] = {"relu", "leakyrelu",
                                           "tangenthyperbolic", "identity"};

/**
 * @brief
 *
 */
const char *C_RealTypeNames[] = {"float", "double"};

/**
 * @brief
 *
 * @param atype
 * @return ActivationFunctionType
 */
ActivationFunctionType StringToActivation(std::string atype) {
  ActivationFunctionType ret = C_Relu;
  if (atype == "LeakyRelu")
    ret = C_LeakyRelu;
  else if (atype == "TangentHyperbolic")
    ret = C_Tanh;
  return ret;
}

/**
 * @brief
 *
 * @param fout
 * @param indent
 */
template <typename T_Stream, typename T_Real>
void generate_activation_functions(T_Stream &fout) {
  bool is_complex = std::is_same<T_Real, std::complex<double>>::value ||
                    std::is_same<T_Real, std::complex<float>>::value;
  fout << fmt::format(
          "  !$acc routine seq\n"
          "  subroutine Relu(n, xin, xout) bind(C)\n"
          "    use iso_c_binding\n"
          "    implicit none\n"
          "    integer(ip), intent(in) :: n\n"
          "    {0}, intent(in)    :: xin(n)\n"
          "    {0}, intent(out)   :: xout(n)\n"
          "    logical :: mask(n)\n"
          "    xout = xin\n"
          "    mask = {1} .le. 0\n"
          "    where(mask)\n"
          "       xout = 0\n"
          "    end where\n"
          "  end subroutine Relu\n\n"
          "  !$acc routine seq\n"
          "  subroutine LeakyRelu(n, xin, xout) bind(C)\n"
          "    use iso_c_binding\n"
          "    implicit none\n"
          "    integer(ip), intent(in) :: n\n"
          "    {0}, intent(in)    :: xin(n)\n"
          "    {0}, intent(out)   :: xout(n)\n"
          "    logical :: mask(n)\n"
          "    xout = xin\n"
          "    mask = {1} .le. 0\n"
          "    where(mask)\n"
          "       xout = {2} * xin\n"
          "    end where\n"
          "  end subroutine LeakyRelu\n\n"
          "  !$acc routine seq\n"
          "  subroutine TangentHyperbolic(n, xin, xout) bind(C)\n"
          "    use iso_c_binding\n"
          "    implicit none\n"
          "    integer(ip), intent(in) :: n\n"
          "    {0}, intent(in)    :: xin(n)\n"
          "    {0}, intent(out)   :: xout(n)\n"
          "    xout = tanh(xin)\n"
          "  end subroutine TangentHyperbolic\n\n",
          code_gen::get_type_string<code_gen::C_Fortran, T_Real>(),
          (is_complex) ? "real(xin)" : "xin",
          (is_complex) ? "0.1" : "0.1d0");
}

/**
 * @brief
 *
 * @param fout
 * @param indent
 */
template <typename T_Stream, typename T_Real>
void generate_activation_functions_cuda(T_Stream &fout) {
  bool is_complex = std::is_same<T_Real, std::complex<double>>::value ||
                    std::is_same<T_Real, std::complex<float>>::value;
  fout << fmt::format(
          "  #pragma acc routine seq\n"
          "  extern inline void zero_array(int n,\n"
          "    {0} x[n]) {{\n"
          "    int i;\n"
          "    //$AD II-LOOP pragma acc loop seq\n"
          "    for(i=0;i<n;++i)\n"
          "      x[i] = 0;\n"
          "  }}\n\n"
          "  #pragma acc routine seq\n"
          "  extern inline void relu(int n,\n"
          "    {0} xin[n],\n"
          "    {0} xout[n]) {{\n"
          "    int i;\n"
          "    zero_array(n, xout);\n"
          "    //$AD II-LOOP pragma acc loop seq\n"
          "    for(i=0; i<n; ++i) {{\n"
          "      if({1} <= 0 ) xout[i] = 0;\n"
          "      else xout[i] = xin[i];\n"
          "    }}\n"
          "  }}\n\n"
          "  #pragma acc routine seq\n"
          "  extern inline void leakyrelu(int n,\n"
          "    {0} xin[n],\n"
          "    {0} xout[n]) {{\n"
          "    int i;\n"
          "    zero_array(n, xout);\n"
          "    //$AD II-LOOP pragma acc loop seq\n"
          "    for(i=0; i<n; ++i) {{\n"
          "      if({1} <= 0 )\n"
          "        xout[i] = 0.1 * xin[i];\n"
          "      else\n"
          "        xout[i] = xin[i];\n"
          "    }}\n"
          "  }}\n\n"
          "  #pragma acc routine seq\n"
          "  extern inline void tangenthyperbolic(int n,\n"
          "    {0} xin[n], {0} xout[n]) {{\n"
          "    int i;\n"
          "    zero_array(n, xout);\n"
          "    //$AD II-LOOP pragma acc loop seq\n"
          "    for(i=0; i<n; ++i)\n"
          "      xout[i] = tanh(xin[i]);\n"
          "  }}\n\n"
          "  #pragma acc routine seq\n"
          "  extern inline void identity(int n,\n"
          "    {0} xin[n], {0} xout[n]) {{\n"
          "    int i;\n"
          "    zero_array(n, xout);\n"
          "    //$AD II-LOOP pragma acc loop seq\n"
          "    for(i=0; i<n; ++i)\n"
          "      xout[i] = xin[i];\n"
          "  }}\n\n",
          code_gen::get_type_string<code_gen::C_CUDA_C, T_Real>(),
          (is_complex) ? "creal(xin[i])" : "xin[i]");
}

/**
 * @brief
 *
 * @param fout
 * @param indent
 */
template <typename T_Stream, typename T_Real>
void generate_common_functions(T_Stream &fout) {
  bool is_complex = std::is_same<T_Real, std::complex<double>>::value ||
                    std::is_same<T_Real, std::complex<float>>::value;
  fout << fmt::format(
          "  !$acc routine seq\n"
          "  subroutine LayerNorm(n, scal, ofs, xin, xout) bind(C)\n"
          "    use iso_c_binding\n"
          "    implicit none\n"
          "    integer(ip), intent(in) :: n\n"
          "    {0}, intent(in)    :: scal(n)\n"
          "    {0}, intent(in)    :: ofs(n)\n"
          "    {0}, intent(in)    :: xin(n)\n"
          "    {0}, intent(out)   :: xout(n)\n"
          "    {0}, parameter     :: eps = {1}\n"
          "    {0} :: mean\n"
          "    {0} :: var\n"
          "    xout = 0\n"
          "    mean = sum(xin(1:n)) / n\n"
          "    var  = sum((xin(1:n) - mean)**2) / n\n"
          "    xout(1:n) = (xin(1:n) - mean) / (sqrt(var) + eps)\n"
          "    xout(1:n) = xout(1:n) * scal(1:n) + ofs(1:n)\n"
          "  end subroutine LayerNorm\n\n"
          "  !$acc routine seq\n"
          "  subroutine DenseLayer(nin, nout, W, b, xin, xout) bind(C)\n"
          "    use iso_c_binding\n"
          "    implicit none\n"
          "    integer(ip), intent(in) :: nin\n"
          "    integer(ip), intent(in) :: nout\n"
          "    {0}, intent(in)    :: W(nin, nout)\n"
          "    {0}, intent(in)    :: b(nout)\n"
          "    {0}, intent(in)    :: xin(nin)\n"
          "    {0}, intent(out)   :: xout(nout)\n"
          "    integer(ip) :: i, j\n"
          "    xout = 0\n"
          "    do j = 1, nout\n"
          "      do i = 1, nin\n"
          "        xout(j) = xout(j) + W(i, j) * xin(i)\n"
          "      end do\n"
          "    end do\n"
          "    xout = xout + b\n"
          "  end subroutine DenseLayer\n\n",
          code_gen::get_type_string<code_gen::C_Fortran, T_Real>(),
          (is_complex) ? "1.0e-5" : "1.0d-5");
}

/**
 * @brief
 *
 * @param fout
 * @param indent
 */
template <typename T_Stream, typename T_Real>
void generate_common_functions_cuda(T_Stream &fout) {
  fout << fmt::format(
          "  #pragma acc routine seq\n"
          "  extern inline void layernorm(int n, {0} scal[n], \n"
          "    {0} ofs[n], {0} xin[n], {0} xout[n]) {{\n"
          "    {0} eps = 1.0e-5;\n"
          "    {0} mean = 0;\n"
          "    {0} var = 0;\n"
          "    int i;\n"
          "    zero_array(n, xout);\n\n"
          "    //$AD II-LOOP pragma acc loop seq\n"
          "    for(i=0; i<n; ++i)\n"
          "      mean = mean + xin[i];\n"
          "    mean = mean / n;\n\n"
          "    //$AD II-LOOP pragma acc loop seq\n"
          "    for(i=0; i<n; ++i)\n"
          "      var = var + (xin[i] - mean) * (xin[i] - mean);\n"
          "    var = var / n;\n\n"
          "    //$AD II-LOOP pragma acc loop seq\n"
          "    for(i=0; i<n; ++i)\n"
          "      xout[i] = (xin[i] - mean) / (sqrt(var) + eps) * scal[i] + "
          "ofs[i];\n"
          "  }}\n\n"
          "  #pragma acc routine seq\n"
          "  extern inline void denselayer(int nin, int nout, \n"
          "    {0} W[nin*nout], {0} b[nout], {0} xin[nin], \n"
          "    {0} xout[nout]) {{\n"
          "    int i, j;\n"
          "    zero_array(nout, xout);\n"
          "    //$AD II-LOOP pragma acc loop seq\n"
          "    for (i = 0; i < nin; i++) {{\n"
          "      //$AD II-LOOP pragma acc loop seq\n"
          "      for (j = 0; j < nout; j++) {{\n"
          "        xout[j] = xout[j] + W[i * nout + j] * xin[i];\n"
          "      }}\n"
          "    }}\n\n"
          "    //$AD II-LOOP pragma acc loop seq\n"
          "    for(i=0; i<nout; ++i) \n"
          "      xout[i] = xout[i] + b[i];\n"
          "  }}\n\n",
          code_gen::get_type_string<code_gen::C_CUDA_C, T_Real>());
}

/**
 * @brief
 *
 * @param fout
 * @param size
 */
template <typename T_Stream, typename T_Real>
void generate_L1_objective(T_Stream &fout, const int size) {
  fout << fmt::format(
          "  subroutine ObjectiveL1(n, xout, xref, f) bind(C)\n"
          "    use iso_c_binding\n"
          "    implicit none\n"
          "    integer(ip), intent(in) :: n\n"
          "    {0}, intent(in)    :: xout({1})\n"
          "    {0}, intent(in)    :: xref({1})\n"
          "    {0}, intent(inout)  :: f\n"
          "    f = f + sum(abs(xout(1:{1}) - xref(1:{1}) ))\n"
          "  end subroutine ObjectiveL1\n\n",
        code_gen::get_type_string<code_gen::C_Fortran, T_Real>(), size);
}
