/**
 * @file code_gen.hpp
 * @author Pavanakumar Mohanamuraly (mpkumar@cerfacs.fr)
 * @brief Code generation bits
 * @version 0.1
 * @date 2022-12-03
 *
 * @copyright Copyright (c) 2022 CERFACS
 *
 */

#pragma once

#include <algorithm>
#include <array>
#include <complex>
#include <fmt/core.h>
#include <fmt/ranges.h>
#include <map>
#include <sstream>
#include <string>
#include <typeinfo>
#include <vector>

#include "anamika_common.hpp"

/**
 * @brief
 *
 */
namespace code_gen {

/**
 * @brief
 *
 */
struct abstract_variable {

  /**
   * @brief Construct a new abstract variable object
   *
   * @param name
   */
  abstract_variable(const char *name) : m_name(name) {}

  /**
   * @brief Get the declaration object
   *
   * @param itype
   * @return const std::string
   */
  virtual const std::string
  get_declaration(const IntentTypes itype = C_IntentNone) const {
    return "";
  }


  /**
   * @brief Get the declaration object
   *
   * @param itype
   * @return const std::string
   */
  virtual const std::string
  get_acc_data_declaration(const IntentTypes itype = C_IntentNone) const {
    return "";
  }

  /**
   * @brief Get the access object
   *
   * @param idx
   * @return const std::string
   */
  virtual const std::string get_access(const int *idx) const { return ""; }

  /**
   * @brief Get the access object
   *
   * @param idx
   * @return const std::string
   */
  virtual const std::string get_access(const std::vector<int> idx) const {
    return "";
  }

  /**
   * @brief Get the access object
   *
   * @param idx
   * @return const std::string
   */
  virtual const std::string
  get_access(const std::vector<std::string> &idx) const {
    return "";
  }

  /**
   * @brief
   *
   * @return true
   * @return false
   */
  virtual bool is_compound() { return false; };

  /**
   * @brief Destroy the abstract variable object
   *
   */
  virtual ~abstract_variable(){};

  const std::string m_name;
};

/**
 * @brief
 *
 * @tparam T_Language
 * @tparam T
 */
template <CodeLanguage T_Language, typename T>
struct scalar_variable : abstract_variable {

  /**
   * @brief Construct a new variable object
   *
   * @param name
   */
  scalar_variable(const char *name) : abstract_variable(name) {}

  /**
   * @brief Get the declaration object
   *
   * @return const std::string
   */
  const std::string
  get_declaration(const IntentTypes itype = C_IntentNone) const override {
    if (T_Language == C_Fortran)
      return fmt::format("{0} {1} :: {2}", get_type_string<T_Language, T>(),
                         get_intent_string(itype), m_name);
    // Comment out if you do not want const in CUDA C
    else if (T_Language == C_CUDA_C && itype == C_IntentIn)
      return fmt::format("const {0} {1}", get_type_string<T_Language, T>(),
                         m_name);
    else
      throw std::runtime_error(
          "Error: Language type other than CUDA-C or Fortran detected.");
  }

  /**
   * @brief Get the index object
   *
   * @param idx
   * @return const std::string
   */
  const std::string get_access(const int *idx) const override { return m_name; }

  /**
   * @brief Get the access object
   *
   * @param idx
   * @return const std::string
   */
  virtual const std::string
  get_access(const std::vector<int> idx) const override {
    return get_access(idx.data());
  }

  /**
   * @brief Get the index object
   *
   * @param idx
   * @return const std::string
   */
  virtual const std::string
  get_access(const std::vector<std::string> &idx) const override {
    return m_name;
  }

  /**
   * @brief Destroy the scalar variable object
   *
   */
  virtual ~scalar_variable(){};
};

/**
 * @brief
 *
 * @tparam T_Ordering
 * @tparam T_Rank
 */
template <CodeLanguage T_Language, StorageOrdering T_Ordering, typename T,
          int T_Rank>
struct array_variable : abstract_variable {

  /**
   * @brief Construct a new variable object
   *
   * @param name
   */
  array_variable(const char *name) : abstract_variable(name) {}

  /**
   * @brief Construct a new array variable object
   *
   * @param name
   * @param idx
   */
  array_variable(const char *name, const std::vector<std::string> idx)
      : abstract_variable(name) {
    std::copy(idx.begin(), idx.end(), m_dim.begin());
  }

  /**
   * @brief Set the dim object
   *
   * @param dim
   */
  void set_dim(const std::string *dim) {
    std::copy(dim, dim + T_Rank, m_dim.begin());
  }

  /**
   * @brief Set the dim object
   *
   * @param dim
   */
  void set_dim(const std::vector<std::string> &dim) {
    std::copy(dim.begin(), dim.end(), m_dim.begin());
  }

  /**
   * @brief Get the declaration object
   *
   * @return const std::string
   */
  const std::string
  get_declaration(const IntentTypes itype = C_IntentNone) const override {
    if (T_Language == C_Fortran)
      return get_fortran_declaration(itype);
    else if (T_Language == C_CUDA_C)
      return get_c_declaration();
  }

  const std::string
  get_acc_data_declaration(const IntentTypes itype = C_IntentNone) const override {
    if (T_Language == C_Fortran)
      return get_fortran_acc_data_declaration(itype);
    else if (T_Language == C_CUDA_C)
      return get_c_acc_data_declaration();
  }

  /**
   * @brief Get the fortran acc data declaration object
   *
   * @param itype
   * @return const std::string
   */
  const std::string
  get_fortran_acc_data_declaration(const IntentTypes itype = C_IntentNone) const {
    return "not implemented";
  }

  /**
   * @brief Get the c acc data declaration object
   *
   * @return const std::string
   */
  const std::string
  get_c_acc_data_declaration() const {
    if (T_Ordering == C_RowMajorOrdering)
      return fmt::format("{0}[0:{1}]", m_name,
                          fmt::join(m_dim.begin(), m_dim.end(), "][0:"));
    else
      return fmt::format("{0}[0:{1}]", m_name,
                          fmt::join(m_dim.rbegin(), m_dim.rend(), "][0:"));
  }

  /**
   * @brief Get the fortran declaration object
   *
   * @return const std::string
   */
  const std::string
  get_fortran_declaration(const IntentTypes itype = C_IntentNone) const {
    std::string s_dim_arg_begin = (T_Rank >= 1) ? ", dimension(" : "";
    std::string s_dim_arg_end = (T_Rank >= 1) ? ")" : "";
    auto s_ret =
        fmt::format("{0}{1}", get_type_string<T_Language, T>(), s_dim_arg_begin);
    if (T_Ordering == C_ColumnMajorOrdering)
      s_ret.append(
          fmt::format("{}", fmt::join(m_dim.begin(), m_dim.end(), ",")));
    else
      s_ret.append(
          fmt::format("{}", fmt::join(m_dim.rbegin(), m_dim.rend(), ",")));
    s_ret.append(fmt::format("{0} {1} :: {2}", s_dim_arg_end,
                             get_intent_string(itype), m_name));
    return s_ret;
  }

  /**
   * @brief Get the c declaration object
   *
   * @return const std::string
   */
  const std::string get_c_declaration() const {
    if (T_Ordering == C_RowMajorOrdering)
      return fmt::format("{0}{1}[{2}]", get_type_string<T_Language, T>(), m_name,
                         fmt::join(m_dim.begin(), m_dim.end(), "]["));
    else
      return fmt::format("{0}{1}[{2}]", get_type_string<T_Language, T>(), m_name,
                         fmt::join(m_dim.rbegin(), m_dim.rend(), "]["));
  }

  /**
   * @brief Get the index object
   *
   * @param idx
   * @return const std::string
   */
  const std::string get_access(const int *idx) const override {
    std::string s_ret(m_name);
    if (T_Language == C_Fortran) {
      if (T_Rank >= 1)
        s_ret += "(";
      if (T_Ordering == C_ColumnMajorOrdering)
        for (int i = 0; i < T_Rank - 1; ++i)
          s_ret += fmt::format("{}, ", idx[i]);
      else
        for (int i = T_Rank - 1; i > 0; --i)
          s_ret += fmt::format("{}, ", idx[i]);
      if (T_Ordering == C_ColumnMajorOrdering)
        s_ret += fmt::format("{}", idx[T_Rank - 1]);
      else
        s_ret += fmt::format("{}", idx[0]);
      if (T_Rank >= 1)
        s_ret += ")";
    } else if (T_Language == C_CUDA_C) {
      if (T_Ordering == C_RowMajorOrdering)
        for (int i = 0; i < T_Rank; ++i)
          s_ret += fmt::format("[{}]", idx[i]);
      else if (T_Ordering == C_ColumnMajorOrdering)
        for (int i = T_Rank - 1; i >= 0; --i)
          s_ret += fmt::format("[{}]", idx[i]);
    }
    return s_ret;
  }

  /**
   * @brief Get the access object
   *
   * @param idx
   * @return const std::string
   */
  const std::string get_access(const std::vector<int> idx) const override {
    return get_access(idx.data());
  }

  /**
   * @brief Get the index object
   *
   * @param idx
   * @return const std::string
   */
  const std::string
  get_access(const std::vector<std::string> &idx) const override {
    std::string s_ret(m_name);
    if (T_Language == C_Fortran) {
      if (T_Rank >= 1)
        s_ret += "(";
      if (T_Ordering == C_ColumnMajorOrdering)
        for (int i = 0; i < T_Rank - 1; ++i)
          s_ret += idx[i] + ", ";
      else
        for (int i = T_Rank - 1; i > 0; --i)
          s_ret += idx[i] + ", ";
      if (T_Ordering == C_ColumnMajorOrdering)
        s_ret += idx[T_Rank - 1];
      else
        s_ret += idx[0];
      if (T_Rank >= 1)
        s_ret += ")";
    } else if (T_Language == C_CUDA_C) {
      if (T_Ordering == C_RowMajorOrdering)
        for (int i = 0; i < T_Rank; ++i)
          s_ret += "[" + idx[i] + "]";
      else if (T_Ordering == C_ColumnMajorOrdering)
        for (int i = T_Rank - 1; i >= 0; --i)
          s_ret += "[" + idx[i] + "]";
    }
    return s_ret;
  }

  /**
   * @brief Destroy the array variable object
   *
   */
  virtual ~array_variable(){};

  /**
   * @brief
   *
   * @return true
   * @return false
   */
  virtual bool is_compound() override { return true; };

  std::array<std::string, T_Rank> m_dim;
};

/**
 * @brief
 *
 * @tparam T_Language
 * @tparam T_Return
 */
template <CodeLanguage T_Language, typename T_Return> struct function {
  /**
   * @brief Construct a new function object
   *
   * @param name
   */
  function(const char *name) : m_name(name) {}

  /**
   * @brief
   *
   * @return const std::string
   */
  const std::string begin() {
    std::string s_ret;
    if (T_Language == C_Fortran)
      s_ret = begin_fortran();
    if (T_Language == C_CUDA_C)
      s_ret = begin_c();
    return s_ret;
  }

  /**
   * @brief
   *
   * @return const std::string
   */
  const std::string begin_fortran() {
    std::string s_ret("subroutine ");
    s_ret += m_name + "(& \n";
    int num_items = m_in.size();
    for (const auto &item : m_in) {
      s_ret += "  &  " + item.second->m_name;
      if (num_items-- > 1)
        s_ret += ",";
      if (num_items == 0 &&
          (!m_out.empty() || !m_inout.empty() || !m_local.empty()))
        s_ret += ",";
      s_ret += "  &\n";
    }
    num_items = m_out.size();
    for (const auto &item : m_out) {
      s_ret += "  &  " + item.second->m_name;
      if (num_items-- > 1)
        s_ret += ",";
      if (num_items == 0 && (!m_inout.empty() || !m_local.empty()))
        s_ret += ",";
    }
    num_items = m_inout.size();
    for (const auto &item : m_inout) {
      s_ret += "  &  " + item.second->m_name;
      if (num_items-- > 1)
        s_ret += ",";
    }
    s_ret += ") &\n  &  bind(C, name=\"" + m_name + "\")\n";
    s_ret += "  use iso_c_binding\n  implicit none\n";
    for (const auto &item : m_in)
      s_ret += "  " + item.second->get_declaration(C_IntentIn) + "\n";
    for (const auto &item : m_out)
      s_ret += "  " + item.second->get_declaration(C_IntentOut) + "\n";
    for (const auto &item : m_inout)
      s_ret += "  " + item.second->get_declaration(C_IntentInOut) + "\n";
    for (const auto &item : m_local)
      s_ret += "  " + item.second->get_declaration(C_IntentNone) + "\n";
    return s_ret;
  }

  /**
   * @brief
   *
   * @return const std::string
   */
  const std::string begin_c() {
    std::string s_ret = fmt::format("void {}(\n", m_name);
    int num_items = m_in.size();
    for (const auto &item : m_in) {
      s_ret += fmt::format("  {}", item.second->get_declaration(C_IntentIn));
      if (num_items-- > 1)
        s_ret += ",\n";
      if (num_items == 0 &&
          (!m_out.empty() || !m_inout.empty() || !m_local.empty()))
        s_ret += ",\n";
    }
    num_items = m_out.size();
    for (const auto &item : m_out) {
      s_ret += "  " + item.second->get_declaration(C_IntentOut);
      if (num_items-- > 1)
        s_ret += ",\n";
      if (num_items == 0 && (!m_inout.empty() || !m_local.empty()))
        s_ret += ",\n";
    }
    num_items = m_inout.size();
    for (const auto &item : m_inout) {
      s_ret += "  " + item.second->get_declaration(C_IntentInOut);
      if (num_items-- > 1)
        s_ret += ",\n";
      if (num_items == 0 && !m_local.empty())
        s_ret += ",\n";
    }
    num_items = m_inout.size();
    for (const auto &item : m_local) {
      s_ret += "  " + item.second->get_declaration(C_IntentNone) + "\n";
      if (num_items-- > 1)
        s_ret += ",\n";
    }
    s_ret += " ) {\n";
    return s_ret;
  }

  /**
   * @brief
   *
   * @return const std::string
   */
  const std::string call() {
    if (T_Language == C_CUDA_C)
      return fmt::format("  {}", call_c());
    else if (T_Language == C_Fortran)
      return fmt::format("  call {}", call_c());
  }

  /**
   * @brief
   *
   * @return const std::string
   */
  const std::string call_c() {
    std::string s_ret = fmt::format("{}(\n", m_name);
    int num_items = m_in.size();
    for (const auto &item : m_in) {
      s_ret += fmt::format("  {}", item.second->m_name);
      if (num_items-- > 1)
        s_ret += ",\n";
      if (num_items == 0 &&
          (!m_out.empty() || !m_inout.empty() || !m_local.empty()))
        s_ret += ",\n";
    }
    num_items = m_out.size();
    for (const auto &item : m_out) {
      s_ret += "  " + item.second->m_name;
      if (num_items-- > 1)
        s_ret += ",\n";
      if (num_items == 0 && (!m_inout.empty() || !m_local.empty()))
        s_ret += ",\n";
    }
    num_items = m_inout.size();
    for (const auto &item : m_inout) {
      s_ret += "  " + item.second->m_name;
      if (num_items-- > 1)
        s_ret += ",\n";
      if (num_items == 0 && !m_local.empty())
        s_ret += ",\n";
    }
    num_items = m_inout.size();
    for (const auto &item : m_local) {
      s_ret += "  " + item.second->m_name + "\n";
      if (num_items-- > 1)
        s_ret += ",\n";
    }
    s_ret += " );\n";
    return s_ret;
  }

  /**
   * @brief
   *
   * @return const std::string
   */
  const std::string end() {
    std::string s_ret;
    if (T_Language == C_Fortran)
      s_ret = end_fortran();
    if (T_Language == C_CUDA_C)
      s_ret = end_c();
    return s_ret;
  }

  /**
   * @brief
   *
   * @return const std::string
   */
  const std::string end_fortran() {
    return fmt::format("end subroutine {}\n", m_name);
  }

  /**
   * @brief
   *
   * @return const std::string
   */
  const std::string end_c() { return "}\n"; }

  /**
   * @brief Destroy the function object
   *
   */
  ~function() {
    for (auto &item : m_in)
      delete item.second;
    for (auto &item : m_out)
      delete item.second;
    for (auto &item : m_inout)
      delete item.second;
    for (auto &item : m_local)
      delete item.second;
  }

  const std::string m_name;
  std::map<int, abstract_variable *> m_in;
  std::map<int, abstract_variable *> m_out;
  std::map<int, abstract_variable *> m_inout;
  std::map<int, abstract_variable *> m_local;
};

/**
 * @brief
 *
 * @tparam T_Language
 * @tparam T_Ordering
 * @tparam T_Real
 * @tparam T_Encoder
 */
template <CodeLanguage T_Language, StorageOrdering T_Ordering, typename T_Real,
          typename T_Encoder, typename T_PinInfo>
struct encoder_function : function<T_Language, void> {

  /**
   * @brief Construct a new encoder function object
   *
   * @param nodeid
   * @param enc
   */
  encoder_function(int nodeid, const T_Encoder &enc, const T_PinInfo &pininfo)
      : function<T_Language, void>(fmt::format("Encoder{}", nodeid)),
        m_encoder(enc), m_pininfo(pininfo) {
    this->m_in[m_nin++] = new scalar_variable<T_Language, int>("nnodes");
    this->m_in[m_nin++] = new scalar_variable<T_Language, int>("nedges");
    this->m_in[m_nin++] = new array_variable<T_Language, T_Ordering, int, 2>(
        "edgelist", {"nedges", "2"});
  }

  /**
   * @brief Construct a new encoder function object
   *
   * @param name
   * @param enc
   */
  encoder_function(const char *name, const T_Encoder &enc,
                   const T_PinInfo &pininfo)
      : function<T_Language, void>(name), m_encoder(enc), m_pininfo(pininfo) {
    this->m_in[m_nin++] = new scalar_variable<T_Language, int>("nnodes");
    this->m_in[m_nin++] = new scalar_variable<T_Language, int>("nedges");
    this->m_in[m_nin++] = new array_variable<T_Language, T_Ordering, int, 2>(
        "edgelist", {"nedges", "2"});
  }

  /**
   * @brief
   *
   * @tparam T_Rank
   * @param name
   * @param idx
   */
  template <int T_Rank>
  void add_encoder_input(const char *name, const std::vector<std::string> idx) {
    this->m_in[m_nin++] =
        new array_variable<T_Language, T_Ordering, T_Real, T_Rank>(name, idx);
  }

  /**
   * @brief
   *
   * @tparam T_Rank
   * @param name
   * @param idx
   */
  template <int T_Rank>
  void add_encoder_output(const char *name,
                          const std::vector<std::string> idx) {
    this->m_out[m_nout++] =
        new array_variable<T_Language, T_Ordering, T_Real, T_Rank>(name, idx);
  }

  /**
   * @brief
   *
   * @return std::string
   */
  std::string generate_helper() {
    std::stringstream fout;
    if (T_Language == C_Fortran) {
      generate_activation_functions<decltype(fout), T_Real>(fout);
      generate_common_functions<decltype(fout), T_Real>(fout);
    } else if (T_Language == C_CUDA_C) {
      generate_activation_functions_cuda<decltype(fout), T_Real>(fout);
      generate_common_functions_cuda<decltype(fout), T_Real>(fout);
    }
    return fout.str();
  }

  /**
   * @brief Set the up vars object
   *
   */
  void setup_vars(std::set<int> &node_pinids) {
    m_node_pinids = &node_pinids;
    m_in_type = C_FEAT_UNKNOWN;
    m_out_type = C_FEAT_UNKNOWN;
    const auto str_param_size = fmt::format("{}", m_encoder.param_size);
    add_encoder_input<1>("params", {str_param_size.c_str()});
    // Add all the input features
    for (auto &item : node_pinids) {
      if (item > 0) {
        auto found_in_to_outs = m_pininfo.m_pin_in_to_outs.find(item);
        auto found_in_type = m_pininfo.m_pinid_to_type.find(item);
        assert(found_in_to_outs != std::end(m_pininfo.m_pin_in_to_outs));
        assert(found_in_type != std::end(m_pininfo.m_pinid_to_type));
        if (m_in_type == C_FEAT_UNKNOWN)
          m_in_type = found_in_type->second;
        for (auto &pinin : found_in_to_outs->second) {
          auto found_type = m_pininfo.m_pinid_to_type.find(pinin);
          assert(found_type != std::end(m_pininfo.m_pinid_to_type));
          auto found_name = m_pininfo.m_pinid_to_name.find(pinin);
          auto found_dim = m_pininfo.m_pinid_to_dim.find(pinin);
          std::string feat_name;
          if (found_type->second == C_FEAT_EDGE_ASYMMETRIC ||
              found_type->second == C_FEAT_EDGE_SYMMETRIC)
            feat_name = "nedges";
          else if (found_type->second == C_FEAT_NODE)
            feat_name = "nnodes";
          add_encoder_input<2>(
              found_name->second.c_str(),
              {feat_name.c_str(), std::to_string(found_dim->second).c_str()});
        }
      } else {
        std::string feat_name;
        auto found_type = m_pininfo.m_pinid_to_type.find(item);
        auto found_name = m_pininfo.m_pinid_to_name.find(item);
        auto found_dim = m_pininfo.m_pinid_to_dim.find(item);
        if (m_out_type == C_FEAT_UNKNOWN)
          m_out_type = found_type->second;
        if (found_type->second == C_FEAT_EDGE_ASYMMETRIC ||
            found_type->second == C_FEAT_EDGE_SYMMETRIC)
          feat_name = "nedges";
        else if (found_type->second == C_FEAT_NODE)
          feat_name = "nnodes";
        add_encoder_output<2>(
            found_name->second.c_str(),
            {feat_name.c_str(), fmt::format("{}", found_dim->second).c_str()});
      }
    }
  }

  /**
   * @brief
   *
   * @tparam T_Stream
   * @param fout
   * @param cur_offset
   * @param ext_offset
   */
  template <typename T_Stream>
  void generate_mlp_cuda(T_Stream &fout, int &cur_offset, long ext_offset) {

    // Add all the input arrays to shared variable list
    std::vector<std::string> str_shared_var_list;
    for (const auto &item : this->m_in)
      if(item.second->is_compound())
        str_shared_var_list.push_back(item.second->m_name);
    for (const auto &item : this->m_out)
      if(item.second->is_compound())
        str_shared_var_list.push_back(item.second->m_name);
    for (const auto &item : this->m_inout)
      if(item.second->is_compound())
        str_shared_var_list.push_back(item.second->m_name);

    bool has_input_edge_feature = false;
    bool is_edge_symmetric = false;
    int in_offset = 0;
    int num_mlp_vars = m_encoder.encoder_layer_dim + 1;
    auto layer_dim = m_encoder.encoder_dim;
    layer_dim.insert(layer_dim.begin(), m_encoder.input_dim);
    // Get the argument name for the functions
    std::vector<std::string> vname(num_mlp_vars);
    vname.front() = fmt::format("xin");
    vname.back() = fmt::format("xout");
    for (int i = 1; i < num_mlp_vars - 1; ++i) // exclude first and last
      vname[i] = fmt::format("x{}", i);

    std::string str_code;
    // Declare the local variables used for input/output
    // and intermediate layer vars
    for (int i = 0; i < num_mlp_vars; ++i) {
      str_code.append(
          fmt::format("  {0} {1} [{2}];\n",get_type_string<T_Language, T_Real>(),
                      vname[i], layer_dim[i]));
      if (i > 0) {
        // Activation and normalisation temporaries
        str_code.append(
            fmt::format("  {0} {1}_noact[{2}];\n"
                        "  {0} {1}_act[{2}];\n",
                        get_type_string<T_Language, T_Real>(), vname[i],
                        layer_dim[i]));
      }
    }
    str_code.append("  int i, i1, i2, iedge, inode;\n");

    str_code.append("  // Zero-out output array\n");
    // Zero out the output arrays
    for (auto &item : *m_node_pinids) {
      if (item < 0) {
        auto found_type = m_pininfo.m_pinid_to_type.find(item);
        assert(found_type != std::end(m_pininfo.m_pinid_to_type));
        auto found_name = m_pininfo.m_pinid_to_name.find(item);
        auto found_dim = m_pininfo.m_pinid_to_dim.find(item);
        switch (found_type->second) {
          case C_FEAT_EDGE_SYMMETRIC:
          case C_FEAT_EDGE_ASYMMETRIC:
            str_code.append(
                fmt::format("  //$AD II-LOOP pragma acc parallel loop collapse(2) present({1})\n"
                            "  for(iedge = 0; iedge < nedges; ++iedge)\n"
                            "      for(i = 0; i < {0}; ++i)\n"
                            "        {1}{2} = 0;\n\n",
                            found_dim->second, // 0
                            found_name->second.data(), // 1
                            code_gen::get_c_index<T_Ordering>({"iedge", "i"}))); // 2
            break;

          case C_FEAT_NODE:
            str_code.append(
                fmt::format("  //$AD II-LOOP pragma acc parallel loop collapse(2) present({1})\n"
                            "  for(inode = 0; inode < nnodes; ++inode)\n"
                            "      for(i = 0; i < {0}; ++i)\n"
                            "        {1}{2}= 0;\n",
                            found_dim->second, // 0
                            found_name->second.data(), // 1
                            code_gen::get_c_index<T_Ordering>({"inode", "i"}))); // 2
          default:
            // throw std::runtime_error(fmt::format("Unknown feature variable type in pin feature"));
            break;
        }
      }
    }

    switch (m_in_type) {
    case C_FEAT_EDGE_SYMMETRIC:
    case C_FEAT_MIXED_NODE_EDGE_SYMMETRIC:
      is_edge_symmetric = true;
    case C_FEAT_EDGE_ASYMMETRIC:
    case C_FEAT_MIXED_NODE_EDGE_ASYMMETRIC:
      has_input_edge_feature = true;
      str_code.append(fmt::format(
          "  //$AD II-LOOP pragma acc parallel loop gang vector present({0}) private({1}, {2}_noact, {3}_act)\n"
          "  for(iedge=0; iedge < nedges; ++iedge) {{\n"
          "    i1 = edgelist[iedge][0];\n"
          "    i2 = edgelist[iedge][1];\n"
          "    // Copy contents into temporary for MLP\n",
          fmt::join(str_shared_var_list.begin(), str_shared_var_list.end(), ", "),
          fmt::join(vname.begin(), vname.end(), ", "),
          fmt::join( std::next(vname.begin()), vname.end(), "_noact, "),
          fmt::join( std::next(vname.begin()), vname.end(), "_act, ") ));
      for (auto &item : *m_node_pinids) {
        if (item > 0) {
          auto found_in_to_outs = m_pininfo.m_pin_in_to_outs.find(item);
          assert(found_in_to_outs != std::end(m_pininfo.m_pin_in_to_outs));
          for (auto &pinin : found_in_to_outs->second) {
            auto found_type = m_pininfo.m_pinid_to_type.find(pinin);
            assert(found_type != std::end(m_pininfo.m_pinid_to_type));
            auto found_name = m_pininfo.m_pinid_to_name.find(pinin);
            auto found_dim = m_pininfo.m_pinid_to_dim.find(pinin);
            // Copy edge input features to the xin
            if (found_type->second == C_FEAT_EDGE_ASYMMETRIC ||
                found_type->second == C_FEAT_EDGE_SYMMETRIC) {
              str_code.append(fmt::format("    //$AD II-LOOP pragma acc loop seq\n"
                                          "    for(i=0; i < {0}; ++i)\n"
                                          "      xin[i + {1}] = {2}{3};\n",
                                          found_dim->second, // 0
                                          in_offset, // 1
                                          found_name->second.data(), // 2
                                          code_gen::get_c_index<T_Ordering>({"iedge", "i"}))); // 3
              in_offset += found_dim->second;
            } else if (found_type->second == C_FEAT_NODE) {
              str_code.append(fmt::format("    //$AD II-LOOP pragma acc loop seq\n"
                                          "    for(i=0; i < {0}; ++i)\n"
                                          "      xin[i + {1}] = {2}{3};\n",
                                          found_dim->second, // 0
                                          in_offset, // 1
                                          found_name->second.data(), // 2
                                          code_gen::get_c_index<T_Ordering>({"i1", "i"}))); // 3
              in_offset += found_dim->second;
              str_code.append(fmt::format("    //$AD II-LOOP pragma acc loop seq\n"
                                          "    for(i=0; i < {0}; ++i)\n"
                                          "      xin[i + {1}] = {2}{3};\n",
                                          found_dim->second, // 0
                                          in_offset, // 1
                                          found_name->second.data(), // 2
                                          code_gen::get_c_index<T_Ordering>({"i2", "i"}))); // 3
              in_offset += found_dim->second;
            }
          }
        }
      }
      break;

    case C_FEAT_NODE:
      str_code.append(fmt::format(
                      "  //$AD II-LOOP pragma acc parallel loop gang vector present({0}) private({1}, {2}_noact, {3}_act)\n"
                      "  for(inode=0; inode < nnodes; ++inode) {{\n"
                      "    // Copy contents into temporary for MLP\n",
                      fmt::join(str_shared_var_list.begin(), str_shared_var_list.end(), ", "),
                      fmt::join(vname.begin(), vname.end(), ", "),
                      fmt::join( std::next(vname.begin()), vname.end(), "_noact, "),
                      fmt::join( std::next(vname.begin()), vname.end(), "_act, ") ));
      for (auto &item : *m_node_pinids) {
        if (item > 0) {
          auto found_in_to_outs = m_pininfo.m_pin_in_to_outs.find(item);
          assert(found_in_to_outs != std::end(m_pininfo.m_pin_in_to_outs));
          for (auto &pinin : found_in_to_outs->second) {
            auto found_type = m_pininfo.m_pinid_to_type.find(pinin);
            assert(found_type != std::end(m_pininfo.m_pinid_to_type));
            auto found_name = m_pininfo.m_pinid_to_name.find(pinin);
            auto found_dim = m_pininfo.m_pinid_to_dim.find(pinin);
            str_code.append(fmt::format("    //$AD II-LOOP pragma acc loop seq\n"
                                        "    for(i = 0; i < {0}; ++i)\n"
                                        "      xin[i + {1}] = {2}{3};\n",
                                        found_dim->second, // 0
                                        in_offset, // 1
                                        found_name->second.data(), // 2
                                        code_gen::get_c_index<T_Ordering>({"inode", "i"}))); // 3
            in_offset += found_dim->second;
          }
        }
      }
      break;

    default:
      throw std::runtime_error(
          "Error: Input pin feature has mixed type. Check your model ...");
      break;
    }

    // MLP code generation
    for (int i = 0; i < num_mlp_vars - 1; ++i) {
        // Dense linear layer
        str_code.append(fmt::format("    // Layer {0}\n"
                                    "    denselayer({1}, {2}, &(params[{3}]), "
                                    "&(params[{4}]), {5}, {6}_noact);\n",
                                    i + 1, // 0
                                    layer_dim[i], // 1
                                    layer_dim[i + 1], // 2
                                    cur_offset, // 3
                                    cur_offset + layer_dim[i] * layer_dim[i + 1], // 4
                                    vname[i], // 5
                                    vname[i + 1])); // 6
      cur_offset +=
          layer_dim[i] * layer_dim[i + 1] + layer_dim[i + 1]; // Weights + Bias
      // Activation function
      str_code.append(fmt::format(
          "    {0}({1}, {2}_noact, {3}_act);\n",
          C_ActivationFunctionNames[m_encoder.encoder_activation[i]], // 0
          layer_dim[i + 1], // 1
          vname[i + 1], // 2
          vname[i + 1])); // 3
      // Normalisation layer
      if (m_encoder.encoder_normalise[i]) {
        str_code.append(
            fmt::format("    layernorm({0}, &(params[{1}]), &(params[{2}]), "
                        "{3}_act, {4});\n",
                        layer_dim[i + 1], // 0
                        cur_offset, // 1
                        cur_offset + layer_dim[i + 1], // 2
                        vname[i + 1], // 3
                        vname[i + 1])); // 4
        cur_offset += 2 * layer_dim[i + 1];
      } else {
        str_code.append(fmt::format("    //$AD II-LOOP pragma acc loop seq\n"
                                    "    for(i = 0; i < {0}; ++i)\n"
                                    "        {1}[i] = {2}_act[i];\n",
                                    layer_dim[i + 1], // 0
                                    vname[i + 1], // 1
                                    vname[i + 1])); // 2
      }
    }
    // Copy to output
    str_code.append(
        "    // Copy contents from temporary of MLP into output feature\n");
    for (auto &item : *m_node_pinids) {
      if (item < 0) {
        auto found_type = m_pininfo.m_pinid_to_type.find(item);
        assert(found_type != std::end(m_pininfo.m_pinid_to_type));
        auto found_name = m_pininfo.m_pinid_to_name.find(item);
        auto found_dim = m_pininfo.m_pinid_to_dim.find(item);
        switch (found_type->second) {
        case C_FEAT_EDGE_SYMMETRIC:
        case C_FEAT_EDGE_ASYMMETRIC:
          str_code.append(fmt::format("    //$AD II-LOOP pragma acc loop seq\n"
                                      "    for(i = 0; i < {0}; ++i)\n"
                                      "      {1}{2} = xout[i];\n",
                                      found_dim->second, // 0
                                      found_name->second.data(), // 1
                                      code_gen::get_c_index<T_Ordering>({"iedge", "i"}))); // 2
          break;

        case C_FEAT_NODE:
          if (!has_input_edge_feature) {
            str_code.append(fmt::format("    //$AD II-LOOP pragma acc loop seq\n"
                                        "    for(i = 0; i < {0}; ++i)\n"
                                        "      {1}{2} = xout[i];\n",
                                        found_dim->second, // 0
                                        found_name->second.data(), // 1
                                        code_gen::get_c_index<T_Ordering>({"inode", "i"}))); // 2

          } else {
            str_code.append(
                fmt::format("    //$AD II-LOOP pragma acc loop seq\n"
                            "    for(i = 0; i < {0}; ++i) {{\n"
                            "      #pragma acc atomic update\n"
                            "      {1}{2} {3}= xout[i];\n"
                            "    }}\n",
                            found_dim->second, // 0
                            found_name->second.data(), // 1
                            code_gen::get_c_index<T_Ordering>({"i2", "i"}), // 2
                            C_ScatterFunctionNames[m_encoder.scatter_type][0])); // 3
            if (is_edge_symmetric)
              str_code.append(fmt::format(
                  "    //$AD II-LOOP pragma acc loop seq\n"
                  "    for(i = 0; i < {0}; ++i) {{\n"
                  "      #pragma acc atomic update\n"
                  "      {1}{2} {3}= xout[i];\n"
                  "    }}\n",
                  found_dim->second, // 0
                  found_name->second.data(), // 1
                  code_gen::get_c_index<T_Ordering>({"i1", "i"}), // 2
                  C_ScatterFunctionNames[m_encoder.scatter_type][1])); // 3
          }
          break;

        default:
          throw std::runtime_error(
              "Error: Input pin feature has mixed type. Check your model ...");
          break;
        }
      }
    }
    str_code.append("  }\n");
    fout << str_code;
  }

  /**
   * @brief
   *
   * @return std::string
   */
  std::string generate_mlp_code() {
    std::string str_code;
    str_code.append(this->begin());
    int cur_offset = 0;
    long ext_offset = 0;
    {
      std::stringstream cat;
      if (T_Language == C_CUDA_C)
        generate_mlp_cuda(cat, cur_offset, ext_offset);
      str_code.append(cat.str());
    }
    str_code.append(this->end());
    return str_code;
  }

  /**
   * @brief Get the output dim object
   *
   * @return std::string
   */
  std::string get_output_dim() {
    if (m_out_type == C_FEAT_EDGE_ASYMMETRIC ||
        m_out_type == C_FEAT_EDGE_SYMMETRIC)
      return "nedges";
    if (m_out_type == C_FEAT_NODE)
      return "nnodes";
    return "error";
  }

  /**
   * @brief Get the output name object
   *
   * @return std::string
   */
  std::string get_output_name() {
    for (auto &item : *m_node_pinids) {
      if (item < 0) {
        auto found_name = m_pininfo.m_pinid_to_name.find(item);
        return found_name->second;
      }
    }
    return std::string("error");
  }

private:
  int m_nin = 0;
  int m_nout = 0;
  feature_types_t m_in_type = C_FEAT_UNKNOWN;
  feature_types_t m_out_type = C_FEAT_UNKNOWN;
  const T_Encoder &m_encoder;
  const T_PinInfo &m_pininfo;
  std::set<int> *m_node_pinids = nullptr;
};

} // namespace code_gen
