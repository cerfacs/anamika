##
set(imgui_sources)
list(APPEND imgui_sources
  external/imgui/imgui.cpp
  # external/imgui/imgui_demo.cpp
  external/imgui/misc/cpp/imgui_stdlib.cpp
  external/imgui/imgui_draw.cpp
  external/imgui/imgui_tables.cpp
  external/imgui/imgui_widgets.cpp
  external/imgui/backends/imgui_impl_sdl2.cpp
  external/imgui/backends/imgui_impl_opengl3.cpp)

##
set(anamika_sources)
list(APPEND anamika_sources
  ${imgui_sources}
  src/anamika_gui.cpp
  src/anamika_blocks.cpp
  # external/implot/implot_demo.cpp
  external/imnodes/imnodes.cpp
  external/implot/implot.cpp
  external/implot/implot_items.cpp
  external/ImGuiColorTextEdit/TextEditor.cpp
  external/ImGuiFileDialog/ImGuiFileDialog.cpp)

##
add_executable(anamika_gui ${anamika_sources})
target_include_directories(anamika_gui PUBLIC "src/include")
target_include_directories(anamika_gui PUBLIC "external/imgui")
target_include_directories(anamika_gui PUBLIC "external/imgui/backends")
target_include_directories(anamika_gui PUBLIC "external/imnodes")
target_include_directories(anamika_gui PUBLIC "external/implot")
target_include_directories(anamika_gui PUBLIC "external/ImGuiColorTextEdit")
target_include_directories(anamika_gui PUBLIC "external/stb")
target_include_directories(anamika_gui PUBLIC "external/IconFontCppHeaders")
target_include_directories(anamika_gui PUBLIC "external/json")
target_include_directories(anamika_gui PUBLIC "external/ImGuiFileDialog")

##
if(CMAKE_SYSTEM_NAME MATCHES Emscripten)
  message("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
  message("^^^^^^^^^^ Enabling emscripten compile ^^^^^^^^^^^")
  message("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
else()
  find_package(OpenGL REQUIRED)
  if (UNIX)
      if (NOT APPLE)
          find_package(Threads REQUIRED)
          find_package(X11 REQUIRED)
          target_link_libraries(anamika_gui PRIVATE
                  ${CMAKE_THREAD_LIBS_INIT} ${X11_LIBRARIES} ${CMAKE_DL_LIBS})
      endif()
  endif()
  add_subdirectory(external/tiny-process-library)
  add_subdirectory(external/SDL)
endif()

##
target_link_libraries(anamika_gui PUBLIC "$<$<CXX_COMPILER_ID:CXX,GNU>:stdc++fs>")

##
if(CMAKE_SYSTEM_NAME MATCHES Emscripten)
  set(CMAKE_EXECUTABLE_SUFFIX ".html")

  set(emscripten_compile_options)
  list(APPEND emscripten_compile_options
    -fwasm-exceptions
    --no-heap-copy
    -Oz
    -g1
    -Wall
    -Wformat
    -sUSE_SDL=2
    -sUSE_ZLIB=1)

  set(emscripten_link_options)
  list(APPEND emscripten_link_options
     ${emscripten_compile_options}
     --bind
     --emrun
     -sFULL_ES3
     -sFORCE_FILESYSTEM=1
     -sEXPORTED_RUNTIME_METHODS=[FS]
     -sWASM=1
     -sALLOW_MEMORY_GROWTH=1
     -sNO_EXIT_RUNTIME=0
     -sASSERTIONS=1
     --shell-file ${CMAKE_SOURCE_DIR}/html/shell_min.html
     -sTOTAL_MEMORY=1024MB
     -sINITIAL_MEMORY=64MB
     -sELIMINATE_DUPLICATE_FUNCTIONS=1)

  target_compile_options(anamika_gui PUBLIC ${emscripten_compile_options})
  target_link_options(anamika_gui PUBLIC ${emscripten_link_options})
  target_link_libraries(anamika_gui PUBLIC fmt::fmt)
else()
  target_link_libraries(anamika_gui PUBLIC OpenGL::GL SDL2::SDL2 tiny-process-library fmt::fmt)
endif()
