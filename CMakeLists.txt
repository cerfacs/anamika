CMAKE_MINIMUM_REQUIRED (VERSION 3.13)
SET (CMAKE_CXX_STANDARD 11)
SET (CMAKE_CXX_STANDARD_REQUIRED ON)

# Project name and version
PROJECT (anamika VERSION 1.0.0)
# Enable all languages
enable_language(CXX C)
# Enable c++11 standard since we use Lambdas
set(CMAKE_CXX_STANDARD 11)

add_subdirectory(external/fmt)

# Set the cmake macro function folder
list(APPEND CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake")
include(anamika)
# include(test_graph)
