Anamika : AI/Neural Network Modelling Language
===============================================

<table>
<tr>
<th>

</th>
<th>  </th>
</tr>
<tr>
<td>

[image]: doc/figures/mudra_anamika.png
![Alt text][image]

[image1]: doc/figures/anamika_logo.png
![Alt text][image1]

</td>
<td>
We present Anamika a modelling framework for creating AI models using blocks and pipes; inspired from Modellica language for modelling engineering systems using blocks connected by pipes.
</td>
</tr>
</table>
